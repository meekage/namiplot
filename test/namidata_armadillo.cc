#include <armadillo>

#include "data_t.hh"

int main()
{
    arma::mat mat_double
      { {14, 12, 16, 17 }
      , {24, 22, 26, 27 }
      };
    arma::Mat<int> mat_int
      { {14, 12, 16, 17 }
      , {24, 22, 26, 27 }
      };

    std::cout << "Testing mat" << std::endl;
    test_namidata(mat_double);
    std::cout << "Testing Mat<int>" << std::endl;
    test_namidata(mat_int);

    arma::mat mat_single
      { {14, 12, 16, 17 } };
    std::cout << "Testing mat_single" << std::endl;
    test_namidata(mat_single);

    arma::rowvec vec_double
      {14, 12, 16, 17 };
    arma::Row<int> vec_int
      {14, 12, 16, 17 };

    std::cout << "Testing rowvec" << std::endl;
    test_namidata(vec_double);
    std::cout << "Testing Row<int>" << std::endl;
    test_namidata(vec_int);

    std::cout << "Testing ArmaVersion " << arma::arma_version::as_string() << std::endl;
    return err;
}
