/* Test the macros in parameters.hh */
#include "test_framework.hh"

#include "parameters.hh"


PARAMETER_GLOBAL_KEYWORD(int, p_int_1);
PARAMETER_GLOBAL_KEYWORD(int, p_int_2);

class grandparent
{
};


class parent : public grandparent,
PARAMETER_INSERT(p_int_1)
{
PARAMETER_SETUP(p_int_1, 1);

public:
    parent() :
        PARAMETER_CONSTRUCT(p_int_1)
    { }

    template <typename ... Args>
    parent(Assigned_parameter const& keyword_argument, Args ... args)
      : parent(args...)
    {
        keyword_invoked(keyword_argument);
    }
};


class child : public parent,
PARAMETER_INSERT(p_int_2)
{
PARAMETER_SETUP_BASE(parent);
PARAMETER_SETUP(p_int_2, 2);

public:
    child() :
        PARAMETER_CONSTRUCT(p_int_2)
    { }

    template <typename ... Args>
    child(Assigned_parameter const& keyword_argument, Args ... args)
      : child(args...)
    {
        keyword_invoked(keyword_argument);
    }

    int get_p1()
    { return PARAM(p_int_1); }
    int get_p2()
    { return PARAM(p_int_2); }
};


int main()
{
    parent p;
        ASSERT(p.p_int_1 == 1);
    p = parent {p_int_1=3};
        ASSERT(p.p_int_1 == 3);

    child c;
        ASSERT(c.p_int_1 == 1);
        ASSERT(c.p_int_2 == 2);
    c = child {p_int_2=5, p_int_1=4};
        ASSERT(c.p_int_1 == 4);
        ASSERT(c.p_int_2 == 5);

    ASSERT(std::is_base_of_v<PARAMETER_TYPE(p_int_1), parent>);
    ASSERT(!std::is_base_of_v<PARAMETER_TYPE(p_int_2), parent>);
    ASSERT(std::is_base_of_v<PARAMETER_TYPE(p_int_1), child>);
    ASSERT(std::is_base_of_v<PARAMETER_TYPE(p_int_2), child>);

    ASSERT(c.get_p1() == 4);
    ASSERT(c.get_p2() == 5);

    return err;
}
