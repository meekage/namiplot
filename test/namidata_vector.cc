#include "data_t.hh"

#include <vector>

void check_mutex()
{
    constexpr size_t size = 1000000;
    bool data_race = false;
    bool finished = false;

    std::vector<double> vec (size);
    data_t data {&vec};

    std::mutex mtx;
    auto reconstruct_vec = [&vec, &finished, &mtx](bool lock)
        {
            for (size_t j = 0; j < 10; j++) {
                if (lock) mtx.lock();
                    vec.clear();
                    for (size_t i = 0; i < size; i++)
                        vec.push_back(0);
                if (lock) mtx.unlock();
            }
            finished = true;
        };

    // check for YES data races
    std::thread th(reconstruct_vec, false);
    while (!finished) {
        std::lock_guard lg(mtx);
        if (vec.size() != size)
            data_race = true;
    }
    th.join();
    ASSERT(data_race)

    // check for NO data races
    data_race = false;
    finished = false;
    std::thread th_lock(reconstruct_vec, true);
    while (!finished) {
        std::lock_guard lg(mtx);
        if (vec.size() != size)
            data_race = true;
    }
    th_lock.join();
    ASSERT(!data_race)

}


int main()
{
    std::vector<double> vector_double { 4, 2, 6, 7 };
    std::vector<int>    vector_int    { 4, 2, 6, 7 };

    std::cout << "Testing vector<double>" << std::endl;
    test_namidata(vector_double);

    std::cout << "Testing vector<int>" << std::endl;
    test_namidata(vector_int);

    check_mutex();

    return err;
}
