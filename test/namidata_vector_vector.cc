#include <vector>

#include "data_t.hh"

int main()
{
    std::vector<std::vector<double>> mat_double
      { {14, 12, 16, 17 }
      , {24, 22, 26, 27 }
      , {34, 32, 36, 37 }
      };
    std::vector<std::vector<int>> mat_int
      { {14, 12, 16, 17 }
      , {24, 22, 26, 27 }
      , {34, 32, 36, 37 }
      };

    std::cout << "Testing vector<vector<double>>" << std::endl;
    test_namidata(mat_double);
    std::cout << "Testing vector<vector<int>>" << std::endl;
    test_namidata(mat_int);

    return err;
}
