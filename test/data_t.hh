#include <namiplot>

#include <thread>

#include "test_framework.hh"


template <Namidata T>
void test_namidata(T const& cmat)
{
    /** Constructors **/
    data_t<T> empty;
        ASSERT(std::is_same<typename decltype(empty)::data_type, T>());
        ASSERT(empty.size() == 0);
        ASSERT(empty.rows() == 0);
        ASSERT(empty.cols() == 0);
        ASSERT(empty.empty() == true);
        ASSERT(empty.get_ptr());

    data_t value {cmat};
        ASSERT(value.empty() == false);
        ASSERT(value.cols() != 0);
        ASSERT(value.rows() != 0);
        ASSERT(value.size() == value.cols() * value.rows());
        ASSERT(value.get_ptr());

    data_t shared {std::make_shared<T>(cmat)};
        ASSERT(shared.empty() == false);
        ASSERT(shared.cols() != 0);
        ASSERT(shared.rows() != 0);
        ASSERT(shared.size() == shared.cols() * shared.rows());
        ASSERT(shared.get_ptr());

    data_t cptr {&cmat};
        ASSERT(cptr.empty() == false);
        ASSERT(cptr.cols() != 0);
        ASSERT(cptr.rows() != 0);
        ASSERT(cptr.size() == cptr.cols() * cptr.rows());
        ASSERT(!cptr.get_ptr());

    T mat = cmat;
    data_t ptr {&mat};
        ASSERT(ptr.empty() == false);
        ASSERT(ptr.cols() != 0);
        ASSERT(ptr.rows() != 0);
        ASSERT(ptr.size() == ptr.cols() * ptr.rows());
        ASSERT(!ptr.get_ptr());

    data_t copy {shared};
        ASSERT(copy.get_ptr() == shared.get_ptr());

    /** operator = **/

    T no_data {};

    data_t value_assigned{cmat};
    value_assigned = no_data;
        ASSERT(value_assigned.empty());

    data_t shared_assigned{cmat};
    shared_assigned = T {};
        ASSERT(shared_assigned.empty());

    data_t ptr_assigned{cmat};
    ptr_assigned = std::make_shared<T>(no_data);
        ASSERT(ptr_assigned.empty());

    data_t copy_assigned{cmat};
    copy_assigned = empty;
        ASSERT(copy_assigned.get_ptr());

    /** iterators **/

    for (auto element : value) if (element) {}
    //for (auto & element : value) if (element) {} // TODO: Test error (good)
    for (auto const& element : value) if (element) {}

    auto it = value.begin();
    for (unsigned i = 0; i < value.size(); i++)
        ASSERT(it++ != value.end());
    ASSERT(it == value.end());

    auto itr = value.begin_row();
    for (unsigned i = 0; i < value.rows(); i++)
        ASSERT(itr++ != value.end_row());
    ASSERT(itr == value.end_row());


    for (auto        row : rows(value)) {
        for (auto        element : row) if (element) {}
        for (auto      & element : row) if (element) {}
        for (auto const& element : row) if (element) {}
    }
    for (auto      & row : rows(value)) {
        for (auto        element : row) if (element) {}
        for (auto      & element : row) if (element) {}
        for (auto const& element : row) if (element) {}
    }
    for (auto const& row : rows(value)) {
        for (auto        element : row) if (element) {}
        for (auto      & element : row) if (element) {}
        for (auto const& element : row) if (element) {}
    }

    for (auto & row : rows(value))
    {
        auto itc = row.begin();
        for (unsigned j = 0; j < value.cols(); j++)
            ASSERT(itc++ != row.end());
        ASSERT(itc == row.end());
    }

    unsigned counter = 0;
    for (auto & row : rows(value))
        for (double element : row)
            if (element)
                ++counter;
            else
                ++counter;
    ASSERT(counter > 0);
    ASSERT(counter == value.size());

    /** mutex **/

    // Tested only in test_namidata_vector

}


