#include <iostream>
#include <type_traits>

inline int err = 0;

#define ASSERT_VERBOSE
#ifndef ASSERT_VERBOSE
#define ASSERT(...) if(!static_cast<bool>(__VA_ARGS__)) {err++; std::cout \
    << "TEST FAILED:" __FILE__ ":" << __LINE__ << std::endl; }
#else
#define ASSERT(...) if(!static_cast<bool>(__VA_ARGS__)) {err++; std::cout \
    << "TEST FAILED:" __FILE__ ":" << __LINE__ << std::endl; }\
    else std::cout << "TEST OK:" __FILE__ ":" << __LINE__ << std::endl;
#endif

