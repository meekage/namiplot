#include "namiplot"

constexpr auto wait_constant = std::chrono::milliseconds(50);

// TODO: Accept r-values and l-values for p.
X11_window:: X11_window(plot_base_t & p,
                        std::chrono::microseconds period,
                        int win_width = 640,
                        int win_height = 480)
  : pl(p),
    update_period(period),
    active(true)
{

    /*****  X11 Connection  *****/

    // 1. Connect to the X server.
    if ((x11_dpy = XOpenDisplay(NULL)) == NULL)
        throw -1;

    // 2. Create a window.
    Drawable window = XCreateWindow(
            x11_dpy,
            DefaultRootWindow(x11_dpy), // The parent window
            0, 0,                       // Top-left coordinates relative to
                                        // the parent's window
            win_width, win_height,
            0,                          // Width of window's border
            0,                          // Depth
            CopyFromParent,             // Class
            CopyFromParent,             // Visual (Color depth, etc.)
            0,                          // Value mask
            nullptr);                   // Attributes
    XStoreName(x11_dpy, window, "NamiPlot");

    // 3. Choose input events.
    XSelectInput(x11_dpy, window, ExposureMask | StructureNotifyMask);

    // 4. Hendler for delete window.
    wmDeleteMessage = XInternAtom(x11_dpy, "WM_DELETE_WINDOW", False);
    XSetWMProtocols(x11_dpy, window, &wmDeleteMessage, 1);

    // 4. Display the window.
    XMapWindow(x11_dpy, window);

    // 5. Create Cairo Surface
    cairo_sfc = cairo_xlib_surface_create(
            x11_dpy,
            window,
            DefaultVisual(x11_dpy, DefaultScreen(x11_dpy)),
            win_width, win_height);
    cairo_xlib_surface_set_size(cairo_sfc, win_width, win_height);

    /*****  Thread  *****/

    try {
        std::thread th {&X11_window::thread_main, this};
        th.detach();

    } catch (...) {
        cairo_surface_destroy(cairo_sfc);
        XCloseDisplay(x11_dpy);
        throw;
    }

}

X11_window:: X11_window(plot_base_t & p, int win_width, int win_height)
  : X11_window(p,
               std::chrono::hours(24),
               win_width,
               win_height)
{}


X11_window:: X11_window(plot_base_t & p, float frequency, int win_width, int win_height)
  : X11_window(p,
               std::chrono::microseconds(static_cast<long>(1.e6/frequency)),
               win_width,
               win_height)
{}


/** X11 Window controller thread.
 *
 * Window update behavior:
 * The thread will check for X Events every wait_constant milliseconds and
 * update if requested. If coincidentally an animation update is also pending,
 * it is processed in the XEvent. If X does not request update, the thread will
 * update the plot every update_period. If the update takes longer than
 * update_period, the frame will be skipped entirely. This yields consistent
 * update timestamps, but has fewer updates than the processor could achieve:
 * After un update, the thread always waits for the next update_period tick
 * before updating again.
 */
void
X11_window:: thread_main()
{
    // FIXME: Protect against exceptions.
    auto now = std::chrono::steady_clock::now;

    auto next_anim_update = now();
    auto next_x_check = now();

    bool anim_update = true;

    do {
        bool update = false;

        /* Xlib event queue processing */
        while (XPending(x11_dpy))
        {
            XEvent e;
            XNextEvent(x11_dpy, &e);

            switch (e.type)
            {
            case Expose:
                update = true;
                break;
            case ConfigureNotify:
                cairo_xlib_surface_set_size(cairo_sfc,
                                            e.xconfigure.width,
                                            e.xconfigure.height);
                break;
            case ClientMessage:
                if (e.xclient.data.l[0] == (long int)wmDeleteMessage){
                    active = false;
                    continue;
                }
            default:
                break;
            }
        }

        if (anim_update || update)
            pl.render(cairo_sfc);

        XFlush(x11_dpy);

        // Calculate sleep time.
        while (next_anim_update < now())
            next_anim_update += update_period;

        next_x_check = now() + wait_constant;

        anim_update = next_anim_update < next_x_check;

        // Sleep if necessary.
        std::this_thread::sleep_until(
                std::min(next_anim_update, next_x_check) );

    } while (active);

    cairo_surface_destroy(cairo_sfc);
    XCloseDisplay(x11_dpy);
}

X11_window:: ~X11_window()
{
    active = false;
}

