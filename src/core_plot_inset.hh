class plot_inset_t : public plot_base_t,
PARAMETER_INSERT(border_color),
PARAMETER_INSERT(border_width),
PARAMETER_INSERT(margins)
{
PARAMETER_SETUP(border_color, black);
PARAMETER_SETUP(border_width, 1.);
PARAMETER_SETUP(margins,      {25_pct, 25_pct, 25_pct, 25_pct});

private:
    plot_inset_t() :
        PARAMETER_CONSTRUCT(border_color),
        PARAMETER_CONSTRUCT(border_width),
        PARAMETER_CONSTRUCT(margins)
        {}

    template <typename ... Args>
    plot_inset_t(Assigned_parameter const& keyword_argument, Args ... args)
      : plot_inset_t(args...)
    {
        keyword_invoked(keyword_argument);
    }
    

public:

    template <typename P, typename ... Args>
        requires std::is_base_of<plot_base_t, P>::value
    plot_inset_t (P const& inset_pl, Args ... args)
      : plot_inset_t(args...)
    {
        inset_plot = std::make_shared<P>(inset_pl);
    }


    template <typename ... Args>
    plot_inset_t (plot_ptr inset_pl, Args ... args)
      : plot_inset_t(args...)
    {
        inset_plot = inset_pl;
    }

protected:
    void draw(draw_context & cairo) const override;

private:
    plot_ptr inset_plot;
public:
    plot_ptr const& plot() const
    {
        return inset_plot;
    }

};


