#include "imprint.hh"

#include <poppler.h>
#include <iostream>

#include "parameter_types.hh"

//_____________________________________________________ Symbol drawing functions

void symbol_preset_asterisk(cairo_t * dc) {
    cairo_move_to(dc, -0.866,  0.5);
    cairo_line_to(dc,  0.866, -0.5);
    cairo_move_to(dc, -0.866, -0.5);
    cairo_line_to(dc,  0.866,  0.5);
    cairo_move_to(dc,  0, -1);
    cairo_line_to(dc,  0,  1);
}

void symbol_preset_circle(cairo_t * dc) {
    cairo_arc(dc, 0,0, 1, 0.,2.*pi);
    cairo_close_path(dc);
}

void symbol_preset_cross(cairo_t * dc) {
    cairo_move_to(dc, -1, -1);
    cairo_line_to(dc,  1,  1);
    cairo_move_to(dc, -1,  1);
    cairo_line_to(dc,  1, -1);
}

void symbol_preset_diamond(cairo_t * dc) {
    cairo_move_to(dc, -1,  0);
    cairo_line_to(dc,  0,  1);
    cairo_line_to(dc,  1,  0);
    cairo_line_to(dc,  0, -1);
    cairo_close_path(dc);
}

void symbol_preset_plus(cairo_t * dc) {
    cairo_move_to(dc, -1,  0);
    cairo_line_to(dc,  1,  0);
    cairo_move_to(dc,  0, -1);
    cairo_line_to(dc,  0,  1);
}

void symbol_preset_point(cairo_t * dc) {
    cairo_set_line_cap(dc, CAIRO_LINE_CAP_ROUND);
    cairo_line_to(dc, 0, 0);
}

void symbol_preset_square(cairo_t * dc) {
    cairo_rectangle(dc, -1,-1, 2,2);
}

std::array<void (*)(cairo_t *), 7> const symbol_presets = {
    &symbol_preset_plus,
    &symbol_preset_circle,
    &symbol_preset_asterisk,
    &symbol_preset_point,
    &symbol_preset_cross,
    &symbol_preset_square,
    &symbol_preset_diamond
    };


//__________________________________________________________________ PDF_PATTERN

void
PDF_pattern_t:: cairo_mask(cairo_t * cairo)
{
    if (pattern)
    {
        cairo_pattern_t * pat = nullptr;
        { cairo_group cg(cairo, pat);
            cairo_set_source(cairo, pattern);
            cairo_paint(cairo);
        }
        ::cairo_mask(cairo, pat);
        cairo_pattern_destroy(pat);
    }
}

PDF_pattern_t &
PDF_pattern_t:: operator = (PDF_pattern_t const& other)
{
    if (other.pattern)
        pattern = cairo_pattern_reference(other.pattern);
    w = other.w;
    h = other.h;
    return *this;
}

PDF_pattern_t:: PDF_pattern_t(PDF_pattern_t const& other)
{
    *this = other;
}

PDF_pattern_t:: PDF_pattern_t(PDF_pattern_t && other)
{
    pattern = other.pattern;
    other.pattern = nullptr;
    w = other.w;
    h = other.h;
}

PDF_pattern_t:: PDF_pattern_t(std::filesystem::path filename)
{
    // * 1 *  Cairo context
    const double canvas_size = 500.;
    cairo_rectangle_t rect {0, 0, canvas_size, canvas_size};
    cairo_surface_t * surface =
        cairo_recording_surface_create( CAIRO_CONTENT_COLOR_ALPHA, &rect );
    cairo_t * cairo = cairo_create( surface );

    // * 2 *  Poppler import
    // FIXME: Error handling of Poppler functions.
    std::string pdf_file = "file:";
    pdf_file += filename;
    GError * error = nullptr;
    PopplerDocument * PDF = poppler_document_new_from_file(
            pdf_file.c_str(), nullptr, &error );
    if (!PDF) throw std::runtime_error("libpoppler error (does the file "
            "exist?)");
    PopplerPage * PAGE = poppler_document_get_page(PDF, 0);

    poppler_page_get_size(PAGE, &w, &h);
    // cairo_recording_surface_ink_extents() returns all 0.
    if (w > canvas_size || h > canvas_size)
    {
        // FIXME!
        //g_object_unref(G_OBJECT(PAGE));
        //g_object_unref(G_OBJECT(PDF));
        throw std::overflow_error("Imported file does not fit in canvas size");
    }

    // * 3 *  Pattern from poppler
    { cairo_group cg(cairo, pattern);
        poppler_page_render(PAGE, cairo);
    }
    cairo_destroy(cairo);
    cairo_surface_destroy(surface);

    // FIXME!
    //g_object_unref(G_OBJECT(PAGE));
    //g_object_unref(G_OBJECT(PDF));
}


PDF_pattern_t:: ~PDF_pattern_t()
{
    if (pattern)
        cairo_pattern_destroy(pattern);
}


