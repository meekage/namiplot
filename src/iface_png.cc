#include "namiplot"

void
plot_to_PNG(
        plot_base_t const& p,
        std::filesystem::path output,
        int png_width,
        int png_height
        )
{
    cairo_surface_t * cairo_sfc =
        cairo_image_surface_create(CAIRO_FORMAT_ARGB32, png_width, png_height);

    p.render(cairo_sfc);

    cairo_status_t error_code =
        cairo_surface_write_to_png(cairo_sfc, output.c_str());

    cairo_surface_destroy(cairo_sfc);

    if (error_code != CAIRO_STATUS_SUCCESS)
    {
        std::string error = "plot_to_PNG() failed: ";
        error += cairo_status_to_string(error_code);
        throw std::runtime_error(error);
    }
}

