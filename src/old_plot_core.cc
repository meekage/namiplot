#include "plot_core.hh"

#include <cairo/cairo-xlib.h>

#include <cmath>
#include <iostream>
#include <numeric>

//_________________________________________________________________________ PLOT

void
plot:: static_draw(draw_context & dc, plot & pl)
{
    if (pl.rebuild_caches(dc))
        pl.draw(dc);
}

bool
plot:: rebuild_caches(draw_context &)
{
    return true;
}

void
plot:: update(cairo_surface_t * surface)
{
    draw_context dc;
    switch (cairo_surface_get_type(surface))
    {
    case CAIRO_SURFACE_TYPE_XLIB:
        dc.width  = cairo_xlib_surface_get_width(surface);
        dc.height = cairo_xlib_surface_get_height(surface);
        break;
    default:
        throw "plot_core.cc: Cairo surface type not supported.";
    }
    dc.cairo = cairo_create(surface);

    { cairo_group cg(dc);
        // Prepare the cairo surface with a white background
        cairo_set_source_rgba(dc, white);
        cairo_paint(dc);

        // Draw all the plots
        static_draw(dc, *this);
    }

    cairo_destroy(dc);
}


//________________________________________________________________________ INSET

void
inset:: draw(draw_context & dc)
{
    double l = margins.lft.value(dc.width);
    double r = (1._pu - margins.rgt).value(dc.width);
    double t = margins.top.value(dc.height);
    double b = (1._pu - margins.bot).value(dc.height);
    double w = r - l;
    double h = b - t;

    // Plot the inset
    cairo_surface_t * draw_surface = cairo_surface_create_for_rectangle(
            cairo_get_group_target(dc), l, t, w, h );
    draw_context inset_ctxt = { cairo_create(draw_surface), (int) w, (int) h };

    plot::static_draw(inset_ctxt, *inset_plot);

    cairo_destroy(inset_ctxt.cairo);
    cairo_surface_destroy(draw_surface);

    // Border
    cairo_set_source_rgba(dc, border_color);
    cairo_set_line_width(dc, border_width);
    cairo_rectangle(dc, l, t, w, h);
    cairo_stroke(dc);
}


//_____________________________________________________________________ SUBPLOTS
