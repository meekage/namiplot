/*_________________________________ CONCEPTS _________________________________*/

class assigned_parameter_base_t
{ /* Empty */ };


// The keyword argument has been assigned to a value
template <typename AP>
concept Assigned_parameter =
    std::is_base_of<assigned_parameter_base_t, AP>::value &&
    std::is_constructible<AP, typename AP::value_type>::value &&
    requires (AP a) {
        a.value;
    };

template <typename D>
concept Namidata_argument =
    ! Assigned_parameter<D> &&
    requires (D d) {
        data_t(d);
    };

/*___________________________ ASSIGNED PARAMETERS ____________________________*/


template <int unique_type_id, typename T>
struct assigned_parameter_t : assigned_parameter_base_t
{
    using value_type = T;
    T const& value;
    assigned_parameter_t(T const& value_arg)
        : value(value_arg)
    {}
};


/*__________________________ UNASSIGNED PARAMETERS ___________________________*/


template <int unique_type_id, typename T>
struct unassigned_parameter_t
{
    using value_type = T;
    using assigned_type = assigned_parameter_t<unique_type_id, T>;
    assigned_parameter_t<unique_type_id, T> operator = (T const& val) const
    {
        return {val};
    }
};

// FIXME Use string literals, i.e. #NAME, instead of (int)__COUNTER__.
#define PARAMETER_GLOBAL_KEYWORD(TYPE, NAME) \
    inline unassigned_parameter_t<__COUNTER__, TYPE> NAME


/*___________________________ STANDARD PARAMETERS ____________________________*/


// FIXME: Copy constructor must copy the correct pointer

template <typename UP> /* UP = unassigned_parameter_t<int, T> */
class parameter_t
{
    UP::value_type const* user_value;
protected:
    parameter_t() = default;

    parameter_t(UP::value_type const* user_value_arg)
      : user_value{user_value_arg}
    { }

    void keyword_invoked(UP::assigned_type const& arg) const
    {
        *const_cast<UP::value_type*>(user_value) = arg.value;
    }

public:

    UP::value_type const& value() const
    {
        return *user_value;
    }
};

#define PARAMETER_INSERT(X) public parameter_t<decltype(::X)>

#define PARAMETER_SETUP_BASE(BASE) \
    protected: using BASE::keyword_invoked
#define PARAMETER_SETUP(X, ... /* DEFAULT */) \
    protected: using parameter_t<decltype(::X)>::keyword_invoked; \
    public: decltype(::X)::value_type X = __VA_ARGS__

#define PARAMETER_CONSTRUCT(X) parameter_t<decltype(::X)>(&X)

#define PARAMETER_TYPE(X) parameter_t<decltype(::X)>
#define PARAM(X) parameter_t<decltype(::X)>::value()


