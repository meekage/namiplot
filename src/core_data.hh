template <typename D>
concept Namidata_vector =
       std::is_default_constructible_v<D>
    && std::is_copy_constructible_v<D>
    && std::is_copy_assignable_v<D>
    && requires (D d) {
           // FIXME: No compiler support yet for:
           // {d[size_t(0)]} -> std::convertible_to<double>;
           // FIXME: This should work but does not:
           // {d[D::size_type(0)]} -> double;
           // FIXME: Actually I don't really need it:
           //{d[size_t(0)]} -> double;
           {d.size()}   -> unsigned;
           {d.empty()}  -> bool;

           {d.begin()}  -> D::const_iterator;
           {d.end()}    -> D::const_iterator;
           {*d.begin()} -> double;
           {*d.end()}   -> double;
       }
    && requires (D lhs, D rhs) {
           {lhs == rhs} -> bool;
           {lhs != rhs} -> bool;
       };


template <typename D>
concept Namidata_vector_vector =
       std::is_default_constructible_v<D>
    && std::is_copy_constructible_v<D>
    && std::is_copy_assignable_v<D>
    && requires (D d) {
           {d.size()}   -> unsigned;
           {d.empty()}  -> bool;

           {d.begin()}  -> D::const_iterator;
           {d.end()}    -> D::const_iterator;
           {*d.begin()} -> Namidata_vector;
           {*d.end()}   -> Namidata_vector;
       }
    && requires (D lhs, D rhs) {
           {lhs == rhs} -> bool;
           {lhs != rhs} -> bool;
       };



template <typename D>
concept Namidata_armadillo =
       std::is_default_constructible_v<D>
    && std::is_copy_constructible_v<D>
    && std::is_copy_assignable_v<D>
    && requires (D d) {
           {d.n_elem}    -> unsigned;
           {d.n_rows}    -> unsigned;
           {d.n_cols}    -> unsigned;
           {d.is_empty()} -> bool;

           {d.begin()}   -> D::const_iterator;
           {d.end()}     -> D::const_iterator;
           {*d.begin()}  -> double;
           {*d.end()}    -> double;

           {d.begin_row(0)}   -> D::const_row_iterator;
           {d.end_row(0)}     -> D::const_row_iterator;
           {*d.begin_row(0)}  -> double;
           {*d.end_row(0)}    -> double;

       }
    && requires (D lhs, D rhs) {
           {approx_equal(lhs, rhs, "reldiff", 0.001)}  -> bool;
       };



/*_________________________________ NAMIDATA _________________________________*/


template <typename D>
concept Namidata =
       Namidata_vector<D>
    || Namidata_vector_vector<D>
    || Namidata_armadillo<D>;


template <Namidata DATA>
class data_t
{ };


/*** Deduction guidelines for all specializations of data_t ***/

template <Namidata DATA>
data_t(DATA const& copy_data)
    -> data_t<DATA>;

template <Namidata DATA>
data_t(std::shared_ptr<DATA> const& shared_data)
    -> data_t<DATA>;

template <Namidata DATA>
data_t(DATA const* observable_data)
    -> data_t<DATA>;


/*_____________________________ ITERATOR ADAPTORS ____________________________*/


/* Enables using range-based for loops with row interators
 * for (auto row : rows(matrix))
 */
template <typename D>
class rows
{
    D const & data;
public:
    rows(D const & data_var)
      : data(data_var)
    { }

    decltype(data.begin_row()) begin() const
    {
        return data.begin_row();
    }

    decltype(data.end_row()) end() const
    {
        return data.end_row();
    }
};

