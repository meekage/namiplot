//________________________________________________________________ COORDINATE_1D

// Screen coordinate.
class coordinate_1D
{
private:
    enum coordinate_type { PIXEL, PERUNIT };
    coordinate_1D(double value, coordinate_type t);

    /* *** Literal operators are friends. *** */

    friend coordinate_1D operator "" _px  (long double);
    friend coordinate_1D operator "" _pct (long double);
    friend coordinate_1D operator "" _pu  (long double);

    friend coordinate_1D operator "" _px  (unsigned long long int);
    friend coordinate_1D operator "" _pct (unsigned long long int);
    // Commented because per unit ranges from 0. to 1.
//  friend coordinate_1D operator "" _pu  (unsigned long long int);

public: /* Constructors */
    coordinate_1D() = default;

public: /* Operators */

    coordinate_1D &
    operator += (coordinate_1D const& s2)
    {
        pixels  += s2.pixels;
        perunit += s2.perunit;
        return *this;
    }

    coordinate_1D &
    operator -= (coordinate_1D const& s2)
    {
        pixels  -= s2.pixels;
        perunit -= s2.perunit;
        return *this;
    }

    // Unary negation mirrors the coordinate along the center of the canvas.
    friend
    coordinate_1D
    operator - (coordinate_1D s1)
    {
        s1.pixels  = -s1.pixels;
        s1.perunit = 1. - s1.perunit;
        return s1;
    }

    coordinate_1D &
    operator *= (double factor)
    {
        pixels  *= factor;
        perunit *= factor;
        return *this;
    }


    friend
    bool operator == (coordinate_1D const& lhs, coordinate_1D const& rhs)
    {
        return lhs.pixels == rhs.pixels
            && lhs.perunit == rhs.perunit;
    }

    friend
    bool operator != (coordinate_1D const& lhs, coordinate_1D const& rhs)
    { return !(lhs == rhs); }


public: /* Public members */
    double pixels     = 0;
    double perunit    = 0;

    /* *** Accessors *** */

    double value    (unsigned spread) const;
    double value_neg(unsigned spread) const;

};

/* ***************  OPERATORS *************** */

inline coordinate_1D   operator +(coordinate_1D s1, coordinate_1D const& s2)
{ return s1 += s2; }

inline coordinate_1D   operator -(coordinate_1D s1, coordinate_1D const& s2)
{ return s1 -= s2; }

inline coordinate_1D   operator *(coordinate_1D s1, double factor)
{ return s1 *= factor; }

inline coordinate_1D   operator *(double factor, coordinate_1D s2)
{ return s2 *= factor; }

/* ***************  LITERALS *************** */

inline coordinate_1D operator""_pct (long double percentage)
{ return coordinate_1D(percentage / 100., coordinate_1D::PERUNIT); }

inline coordinate_1D operator""_pct (unsigned long long int percentage)
{
    return coordinate_1D(
            static_cast<double>(percentage) / 100., coordinate_1D::PERUNIT);
}


inline coordinate_1D operator""_pu (long double perunit)
{ return coordinate_1D(perunit, coordinate_1D::PERUNIT); }

// Integers do not make much sense here because per unit ranges from 0. to 1.
//coordinate_1D operator""_pu (unsigned long long int perunit)
//{ return coordinate_1D(perunit, coordinate_1D::PERUNIT); }


inline coordinate_1D operator""_px (long double pixels)
{ return coordinate_1D(pixels, coordinate_1D::PIXEL); }

inline coordinate_1D operator""_px (unsigned long long int pixels)
{ return coordinate_1D(pixels, coordinate_1D::PIXEL); }


/* ***************  CAIRO OVERLOADS *************** */

inline void
cairo_move_to(draw_context  const& cairo,
              coordinate_1D const& x,
              coordinate_1D const& y)
{
    cairo_move_to(cairo,
                  x.value(cairo.width),
                  y.value(cairo.height));
}
inline void
cairo_move_to(draw_context  const& cairo,
              double               x,
              coordinate_1D const& y)
{
    cairo_move_to(cairo, x, y.value(cairo.height));
}
inline void
cairo_move_to(draw_context  const& cairo,
              coordinate_1D const& x,
              double               y)
{
    cairo_move_to(cairo, x.value(cairo.width), y);
}

inline void
cairo_line_to(draw_context  const& cairo,
              coordinate_1D const& x,
              coordinate_1D const& y)
{
    cairo_line_to(cairo,
                  x.value(cairo.width),
                  y.value(cairo.height) );
}
inline void
cairo_line_to(draw_context  const& cairo,
              double               x,
              coordinate_1D const& y)
{
    cairo_line_to(cairo, x, y.value(cairo.height));
}
inline void
cairo_line_to(draw_context  const& cairo,
              coordinate_1D const& x,
              double               y)
{
    cairo_line_to(cairo, x.value(cairo.width), y);
}

//________________________________________________________________ COORDINATE_2D

class coordinate_2D
{
public:
    coordinate_1D x, y;

    double norm(draw_context & cairo) const;

public:
    coordinate_2D() = default;

    coordinate_2D(coordinate_1D const& x_coord, coordinate_1D const& y_coord)
      : x(x_coord), y(y_coord)
    { }

    /* *** Operators *** */

    coordinate_2D &
    operator += (coordinate_2D const& s2)
    {
        x += s2.x;
        y += s2.y;
        return *this;
    }

    friend
    coordinate_2D
    operator - (coordinate_2D s1)
    {
        s1.x = -s1.x;
        s1.y = -s1.y;
        return s1;
    }

    coordinate_2D
    operator *=(double factor)
    {
        x *= factor;
        y *= factor;
        return *this;
    }
};

inline coordinate_2D & operator -=(coordinate_2D & s1, coordinate_2D const& s2)
{ return s1 += -s2; }

inline coordinate_2D   operator +(coordinate_2D s1, coordinate_2D const& s2)
{ return s1 += s2; }

inline coordinate_2D   operator -(coordinate_2D s1, coordinate_2D const& s2)
{ return s1 += -s2; }

inline coordinate_2D   operator *(coordinate_2D s1, double factor)
{ return s1 *= factor; }

inline coordinate_2D   operator *(double factor, coordinate_2D s2)
{ return s2 *= factor; }

/* ***************  CAIRO OVERLOADS *************** */

void cairo_move_to(draw_context const& cairo, coordinate_2D const& point);
void cairo_line_to(draw_context const& cairo, coordinate_2D const& point);


//___________________________________________________________________ VIEW_LIMIT


enum view_limit_types_t
{
    //auto_int_tight,
    //auto_int_tick,
    //auto_trig_tight,
    //auto_trig_tick,
    //square_tight,
    //square_tick,
    //fullauto,
    autom,
    square,
};


class view_limit_t
{
    mutable double a = 1.;
    mutable double b = 0.;

public:
    std::variant<view_limit_types_t, double> min;
    std::variant<view_limit_types_t, double> max;


public:
    view_limit_t();

    view_limit_t(view_limit_types_t autom_limit);

    template <typename MIN, typename MAX>
    view_limit_t(MIN const& min_arg, MAX const& max_arg);


public:
    template <Namidata D>
    void calculate_view_limits (data_t<D> const& data, double spread) const;

    double scale (double datum) const;

    double pixel_to_data (double y) const;


public:
    friend
    bool operator == (view_limit_t const& lhs, view_limit_t const& rhs);
};


bool operator != (view_limit_t const& lhs, view_limit_t const& rhs);


/* ********** TEMPLATE FUNCTIONS ********** */


template <Namidata D>
void
view_limit_t:: calculate_view_limits (
        data_t<D> const& data, double spread) const
{
    double mmin, mmax;
    // TODO FIXME Add support for other kind of auto and manual limtis.
    if (min.index() == 0 || max.index() == 0)
    {
        if (!data.size())
            throw std::range_error("Cannot calculate view limits "
                    "because there is no data");

        auto minmax = std::minmax_element(data.begin(), data.end());
        mmin = *minmax.first;
        mmax = *minmax.second;
    }
    else
    {
        mmin = std::get<1>(min);
        mmax = std::get<1>(max);
    }

    if (mmax - mmin <= std::numeric_limits<double>::lowest())
        throw std::range_error("Span of limits is zero. Division by zero");

    // Does not negate the data:
    //a = -spread/(mmax-mmin);
    //b = -a*mmax;
    // Negates the data:
    a = spread / (mmax-mmin);
    b = -a*mmin;
}


template <typename MIN, typename MAX>
view_limit_t:: view_limit_t (MIN const& min_arg, MAX const& max_arg)
  : min(min_arg)
  , max(max_arg)
{ }


//________________________________________________________________ VIEW_LIMIT_2D


class view_limit_2D_t
{
public:
    view_limit_t x;
    view_limit_t y;


public:
    view_limit_2D_t() = default;

    template <typename LIM>
    view_limit_2D_t(LIM const& xylimit)
      : x(xylimit), y(xylimit) {}

    template <typename XLIM, typename YLIM>
    view_limit_2D_t(XLIM const& xlimit, YLIM const& ylimit)
      : x(xlimit), y(ylimit) {}

    template <typename XMIN, typename XMAX, typename YMIN, typename YMAX>
    view_limit_2D_t(XMIN const& xmin, XMAX const& xmax,
                    YMIN const& ymin, YMAX const& ymax)
      : x(xmin, xmax), y(ymin, ymax) {}

public:
    friend
    bool operator == (view_limit_2D_t const& lhs, view_limit_2D_t const& rhs)
    {
        return lhs.x == rhs.x && lhs.y == rhs.y;
    }
};


inline
bool operator != (view_limit_2D_t const& lhs, view_limit_2D_t const& rhs)
{
    return !(lhs == rhs);
}


//______________________________________________________________________ MARGINS


using margins_t = sides<coordinate_1D>;


//________________________________________________________________________ ALIGN


enum text_align_t
{
    NW,  // Relative to font dimensions.
    N,
    NE,
    W,
    C,
    E,
    SW,
    S,
    SE,

    BBOX_NW,  // Relative to the rendered text bounding box.
    BBOX_N,
    BBOX_NE,
    BBOX_W,
    BBOX_C,
    BBOX_E,
    BBOX_SW,
    BBOX_S,
    BBOX_SE,
};


//________________________________________________________________________ COLOR


// RGB
constexpr uint32_t lightred     = 0xFFA0A0;
constexpr uint32_t lightgreen   = 0xA0FFA0;
constexpr uint32_t lightblue    = 0xA0A0FF;
constexpr uint32_t red          = 0xFF0000;
constexpr uint32_t green        = 0x00FF00;
constexpr uint32_t blue         = 0x0000FF;
constexpr uint32_t darkred      = 0xA00000;
constexpr uint32_t darkgreen    = 0x00A000;
constexpr uint32_t darkblue     = 0x0000A0;

// CMY
constexpr uint32_t lightcyan    = 0xA0FFFF;
constexpr uint32_t lightmagenta = 0xFFA0FF;
constexpr uint32_t lightyellow  = 0xFFFFA0;
constexpr uint32_t cyan         = 0x00FFFF;
constexpr uint32_t magenta      = 0xFF00FF;
constexpr uint32_t yellow       = 0xFFFF00;
constexpr uint32_t darkcyan     = 0x00A0A0;
constexpr uint32_t darkmagenta  = 0xA000A0;
constexpr uint32_t darkyellow   = 0xA0A000;

// B/W
constexpr uint32_t white        = 0xFFFFFF;
constexpr uint32_t lightgrey    = 0xC0C0C0;
constexpr uint32_t grey         = 0x808080;
constexpr uint32_t darkgrey     = 0x404040;
constexpr uint32_t black        = 0x000000;

// Others
constexpr uint32_t aqua         = 0x7FFFD4;
constexpr uint32_t beige        = 0xF5F5DC;
constexpr uint32_t brown        = 0x964B00;
constexpr uint32_t lime         = 0xBFFF00;
constexpr uint32_t maroon       = 0x800000;
constexpr uint32_t navy         = 0x000080;
constexpr uint32_t olive        = 0x808000;
constexpr uint32_t orange       = 0xFFA500;
constexpr uint32_t peach        = 0xFFDAB9;
constexpr uint32_t pink         = 0xFFC0CB;
constexpr uint32_t purple       = 0x800080;
constexpr uint32_t silver       = 0xC0C0C0;
constexpr uint32_t snow         = 0xF8F8FF;
constexpr uint32_t teal         = 0x008080;
constexpr uint32_t violet       = 0xA020F0;

// Matlab colors
constexpr uint32_t matblue      = 0x0072BD;
constexpr uint32_t matorange    = 0xD95319;
constexpr uint32_t matyellow    = 0xEDB120;
constexpr uint32_t matpurple    = 0x7E2F8E;
constexpr uint32_t matgreen     = 0x77AC30;
constexpr uint32_t matcyan      = 0x4DBEEE;
constexpr uint32_t matred       = 0xA2142F;
constexpr uint32_t matgrey      = 0xF0F0F0;


class color_t
{
public:
    color_t();
    color_t(uint32_t hex);
    color_t(uint32_t hex, float alpha);
    color_t(float red, float green, float blue);
    color_t(float red, float green, float blue, float alpha);

    /* Red, Green, Blue and Alpha */
    float r,g,b,a;

    friend
    bool operator == (color_t const& lhs, color_t const& rhs);
};


bool operator != (color_t const& lhs, color_t const& rhs);


// FIXME: Linker error
//const color_t transparent {0x000000, 0.};

/* ***************  CAIRO OVERLOADS *************** */

inline void
cairo_set_source_rgb(cairo_t *cr, color_t const& color)
{
    if (color.a > 0.)
        cairo_set_source_rgb(cr, color.r, color.g, color.b);
}
inline void
cairo_set_source_rgba(cairo_t *cr, color_t const& color)
{
    cairo_set_source_rgba(cr, color.r, color.g, color.b, color.a);
}



//____________________________________________________________________ LINE_DASH


const std::vector<double> solid   = { };
const std::vector<double> dashed  = {9, 6};
const std::vector<double> dotted  = {1, 2};
const std::vector<double> dashdot = {6, 3, 1, 3};


//__________________________________________________________________ LATEX_STYLE


enum math_style_t {
    inlinemath,
    displaymath,
};

