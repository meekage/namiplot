#include "plot_text.hh"

//_________________________________________________________________________ TEXT

void
text:: angle_alignment (double radians)
{
    while   (radians < 0)       radians += 2*pi;
    while   (radians >= 2*pi)   radians -= 2*pi;

         if (radians < pi/8)    align = BBOX_W;
    else if (radians < 3*pi/8)  align = SW;
    else if (radians < 5*pi/8)  align = S;
    else if (radians < 7*pi/8)  align = SE;
    else if (radians < 9*pi/8)  align = BBOX_E;
    else if (radians < 11*pi/8) align = NE;
    else if (radians < 13*pi/8) align = N;
    else if (radians < 15*pi/8) align = NW;
    else                        align = BBOX_W;
}

void
text:: draw(draw_context & dc)
{
    // TODO: Use FAST antialias (see cairo_font_options_set_antialias) when
    //       rendering animations.

    cairo_set_source_rgba(dc, color);
    cairo_set_font_size(dc, size);
    cairo_select_font_face(dc,
                           "sans-serif",
                           CAIRO_FONT_SLANT_NORMAL,
                           CAIRO_FONT_WEIGHT_NORMAL);

    // Calculate relative translation of the text origin for alignment.
    double xrel = 0, yrel = 0;
    cairo_text_extents_t t_ext;
    cairo_font_extents_t f_ext;

    cairo_text_extents(dc, text_string.c_str(), &t_ext);
    cairo_font_extents(dc, &f_ext);

    switch(align)
    {
    case BBOX_NW:
        xrel = t_ext.x_bearing;
        yrel = t_ext.y_bearing;
        break;
    case BBOX_N:
        xrel = t_ext.x_bearing + t_ext.width/2;
        yrel = t_ext.y_bearing;
        break;
    case BBOX_NE:
        xrel = t_ext.x_bearing + t_ext.width;
        yrel = t_ext.y_bearing;
        break;
    case BBOX_W:
        xrel = t_ext.x_bearing;
        yrel = t_ext.y_bearing + t_ext.height/2;
        break;
    case BBOX_C:
        xrel = t_ext.x_bearing + t_ext.width/2;
        yrel = t_ext.y_bearing + t_ext.height/2;
        break;
    case BBOX_E:
        xrel = t_ext.x_bearing + t_ext.width;
        yrel = t_ext.y_bearing + t_ext.height/2;
        break;
    case BBOX_SW:
        xrel = t_ext.x_bearing;
        yrel = t_ext.y_bearing + t_ext.height;
        break;
    case BBOX_S:
        xrel = t_ext.x_bearing + t_ext.width/2;
        yrel = t_ext.y_bearing + t_ext.height;
        break;
    case BBOX_SE:
        xrel = t_ext.x_bearing + t_ext.width;
        yrel = t_ext.y_bearing + t_ext.height;
        break;

    case NW:
        xrel = t_ext.x_bearing;
        yrel = -f_ext.ascent;
        break;
    case N:
        xrel = t_ext.x_bearing + t_ext.width/2;
        yrel = -f_ext.ascent;
        break;
    case NE:
        xrel = t_ext.x_bearing + t_ext.width;
        yrel = -f_ext.ascent;
        break;
    case W:
        xrel = t_ext.x_bearing;
        yrel = 0;
        break;
    case C:
        xrel = t_ext.x_bearing + t_ext.width/2;
        yrel = 0;
        break;
    case E:
        xrel = t_ext.x_bearing + t_ext.width;
        yrel = 0;
        break;
    case SW:
        xrel = t_ext.x_bearing;
        yrel = f_ext.descent;
        break;
    case S:
        xrel = t_ext.x_bearing + t_ext.width/2;
        yrel = f_ext.descent;
        break;
    case SE:
        xrel = t_ext.x_bearing + t_ext.width;
        yrel = f_ext.descent;
        break;
    default:
        break;
    };

    cairo_matrix_t matrix;
    cairo_matrix_init_identity(&matrix);

    cairo_matrix_translate(&matrix,
            xy.x.value(dc.width), xy.y.value(dc.height));
    cairo_matrix_rotate(&matrix, -angle);
    cairo_matrix_translate(&matrix, -xrel, -yrel);

    cairo_set_matrix(dc, &matrix);
    cairo_show_text(dc, text_string.c_str());
    cairo_identity_matrix(dc);

    cairo_new_path(dc);
}

coordinate_2D
text:: extents(cairo_t * cairo)
{
    cairo_text_extents_t t_ext;
    { cairo_substate ss (cairo);
        cairo_set_font_size(cairo, size);
        cairo_select_font_face(cairo,
                "sans-serif",
                CAIRO_FONT_SLANT_NORMAL,
                CAIRO_FONT_WEIGHT_NORMAL);
        cairo_text_extents(cairo, text_string.c_str(), &t_ext);
    }

    coordinate_2D ret;
    ret.x.pixels = t_ext.width;
    ret.y.pixels = t_ext.height;
    return ret;
}

bool
text:: rebuild_caches(draw_context & dc)
{
    coordinate_2D dims = extents(dc);
    min_width  = dims.x;
    min_height = dims.y;
    return true;
}


//______________________________________________________________ LATEX_EXCEPTION


latex_exception:: latex_exception(std::string const& what_arg,
                                  std::string const& latex_log_arg )
    : std::runtime_error(what_arg),
      latex_log(latex_log_arg)
{ }

latex_exception:: latex_exception(char        const* what_arg,
                                  std::string const& latex_log_arg )
    : std::runtime_error(what_arg),
      latex_log(latex_log_arg)
{ }

//________________________________________________________________________ LATEX

#include <fstream>
#include <iostream>

#include <sys/wait.h>
#include <unistd.h>

void
latex:: draw(draw_context & dc)
{
    const double latex_ratio = 338./318.;
    try
    {
        // FIXME: Why is the transformation matrix not preserved?
        const double factor = latex_ratio * size/12.;
        cairo_scale(dc, factor, factor);

        const double w2 = texpattern.width()  / 2. * factor;
        const double h2 = texpattern.height() / 2. * factor;

        // Adjust location
        double xrel = -w2;
        double yrel = -h2;
        switch(align)
        {
        case BBOX_NW:
        case NW:
            xrel += w2;
            yrel += h2;
            break;
        case BBOX_N:
        case N:
            yrel += h2;
            break;
        case BBOX_NE:
        case NE:
            xrel -= w2;
            yrel += h2;
            break;
        case BBOX_W:
        case W:
            xrel += w2;
            break;
        case BBOX_C:
        case C:
            break;
        case BBOX_E:
        case E:
            xrel -= w2;
            break;
        case BBOX_SW:
        case SW:
            xrel += w2;
            yrel -= h2;
            break;
        case BBOX_S:
        case S:
            yrel -= h2;
            break;
        case BBOX_SE:
        case SE:
            xrel -= w2;
            yrel -= h2;
            break;
        default:
            break;
        };
        cairo_translate(dc,
                (xy.x.value(dc.width)  + xrel) / factor,
                (xy.y.value(dc.height) + yrel) / factor
                );

        cairo_set_source_rgba(dc, color);
        texpattern.cairo_mask(dc);
    }
    catch (std::exception & err)
    {
        std::cout << "namiplot:latex_render():"<< err.what() << std::endl;
    }
}

// TODO: Make async if animation.
// Returns width and height of the rendered LaTeX text.
PDF_pattern_t
latex:: latex_to_pattern()
{
    namespace fs = std::filesystem;

    fs::path    tmp_prefix    = fs::temp_directory_path()/
                    ("namiplot-" + std::to_string(getpid()) + "/");
    std::string filename_root = "namiplot";
    std::string tex_file      = tmp_prefix/ (filename_root+".tex");

    // ** 1 **  Build the tex file.
    {
        fs::create_directory(tmp_prefix);
        std::ofstream namitex
                  {tex_file, std::ios_base::out | std::ios_base::trunc};

        // Remove the equation environment tags if present.
        std::string math_eq = text_string;
        if (math_eq.front() == '$' && math_eq.back() == '$') {
            math_eq.erase(math_eq.begin());
            math_eq.erase(math_eq.end()-1);
        }

        namitex << "\\documentclass[border=0.5pt,12pt]{standalone} \n"
                << header
                << "\\begin{document} \n"
                << ( math_style == inlinemath ? "$ " : "$\\displaystyle " )
                << math_eq
                << " $ \n"
                << "\\end{document} \n" ;
    }

    // ** 2 **  Execute pdflatex.
    try
    {
        int pid = fork();
        if (pid == -1)
                throw std::runtime_error("fork failed");
        if (pid)
        {
            int wstatus = 0;
            if (  waitpid(pid, &wstatus, 0) == -1
               || !WIFEXITED(wstatus)
               || WEXITSTATUS(wstatus) )
            {
                throw std::runtime_error("LaTeX process failed");
            }
        }
        else // The child process
        {
            fs::current_path(tmp_prefix);

            if (!freopen("/dev/null", "r", stdin )) exit(-1);
            if (!freopen("/dev/null", "w", stdout)) exit(-1);
            if (!freopen("/dev/null", "w", stderr)) exit(-1);

            if (!execl( PDFLATEX_COMPILER,
                        PDFLATEX_COMPILER,
                        "-halt-on-error",
                        tex_file.c_str(),
                        nullptr ))
            {
                exit(-1);
            }
        }
    }
    catch (std::exception const& error)
    {
        std::ifstream logfile { tmp_prefix/ (filename_root+".log"),
                                std::ios_base::in };
        std::stringbuf logbuf;
        logfile >> &logbuf;
        std::filesystem::remove_all(tmp_prefix);
        throw latex_exception { error.what(), logbuf.str() };
    }

    // ** 3 **
    PDF_pattern_t latex_text {tmp_prefix/(filename_root+".pdf")};
    std::filesystem::remove_all(tmp_prefix);
    return latex_text;
}

