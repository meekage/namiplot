#include "frame.hh"

#include <iostream>


//________________________________________________________________________ TICKS

void
ticks:: draw(draw_context & dc)
{
    std::vector<coordinate_1D> tick_pos =
            {0._pu, .2_pu, .4_pu, .6_pu, .8_pu, 1._pu};

    cairo_move_to(dc, 0, dc.height);
    cairo_line_to(dc, dc.width, dc.height);
    for (coordinate_1D const& tick : tick_pos)
    {
        cairo_move_to(dc, tick, dc.height);
        cairo_line_to(dc, tick, dc.height - 4.5);
    }
    cairo_set_source_rgba(dc, color);
    cairo_set_line_width(dc, width);
    cairo_stroke(dc);
}


bool
ticks:: rebuild_caches(draw_context &)
{
    return true;
}


//________________________________________________________________________ FRAME


void
frame:: draw(draw_context & dc)
{
    inset::draw(dc);

    double l = margins.lft.value(dc.width);
    double r = (1._pu - margins.rgt).value(dc.width);
    double t = margins.top.value(dc.height);
    double b = (1._pu - margins.bot).value(dc.height);
    double w = r - l;
    double h = b - t;
    // Frame
    if (background.a > 0.)
    {
        cairo_set_source_rgba(dc, background);
        // Rectangles because EPS does not support alpha nor cairo operators.
        // FIXME: Try with cairo clip rectangles.
        cairo_rectangle(dc, 0,0 , l,dc.height);
        cairo_rectangle(dc, r,0 , dc.width,dc.height);
        cairo_rectangle(dc, l,0 , r,t);
        cairo_rectangle(dc, l,b , r,dc.height);
        cairo_fill(dc);
    }

    { cairo_substate ss (dc);
	draw_context sc = dc;
	cairo_translate(sc.cairo, margins.lft.value(sc.width),
		margins.top.value(sc.height));
	sc.width -= margins.lft.value(sc.width)  + margins.rgt.value(sc.width);
	sc.height -= margins.top.value(sc.height) + margins.bot.value(sc.height);
	static_draw(sc, prueba_ticks);
    }

    draw(dc, decorations.lft, margins_t{10_pct, -margins.lft, 0._pu, 0._pu} );
    draw(dc, decorations.rgt, margins_t{-margins.rgt, 10_pct, 0._pu, 0._pu} );
    draw(dc, decorations.top, margins_t{0._pu, 0._pu, 10_pct, -margins.top} );
    draw(dc, decorations.bot, margins_t{0._pu, 0._pu, -margins.bot, 10_pct} );
}

void
frame:: draw(draw_context & dc,
             frame_decorations_ptr decoration,
             margins_t inset_margins)
{
    if (decoration)
    {
        inset decoration_plot{decoration,
                              ::margins=inset_margins, ::border_width=0.};
        static_draw(dc, decoration_plot);
    }
}

bool
frame:: rebuild_caches(draw_context & dc)
{
    margins.lft = 10_pct;
    margins.rgt = 10_pct;
    margins.top = 10_pct;
    margins.bot = 10_pct;

    if (inset_plot)//FIXME:inset::rebuild_caches();
        inset_plot->rebuild_caches(dc);

    rebuild_caches(dc, decorations.lft, margins.lft, dc.width);
    rebuild_caches(dc, decorations.rgt, margins.rgt, dc.width);
    rebuild_caches(dc, decorations.top, margins.top, dc.height);
    rebuild_caches(dc, decorations.bot, margins.bot, dc.height);
    return true;
}

void
frame:: rebuild_caches(draw_context & dc,
                       frame_decorations_ptr decoration,
                       coordinate_1D & margin,
                       double span)
{
    if (decoration) {
        decoration->rebuild_caches(dc);
        margin = 10_pct + decoration->min_height;
        // FIXME: Check if this is needed or not.
        decorations.bot->rebuild_caches(dc);
        decorations.bot->set_min_heights_as_ratios(span);
    }
}
