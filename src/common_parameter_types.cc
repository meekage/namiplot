#include "namiplot"

#include <cmath>


//________________________________________________________________ COORDINATE_1D

coordinate_1D:: coordinate_1D(double value, coordinate_type t)
{
    switch (t)
    {
    case PIXEL:
        pixels = value;
        break;
    case PERUNIT:
        perunit = value;
        break;
    default:
        throw "coordinate_1D(): Unknown coordinate type";
    }
}

double
coordinate_1D:: value(unsigned spread) const
{
    double ret = 0;
    ret += pixels;
    ret += perunit * spread;
    return ret;
}


//________________________________________________________________ COORDINATE_2D

double
coordinate_2D:: norm(draw_context & dc) const
{
    return sqrt(
            pow(x.value(dc.width),2) + pow(y.value(dc.height),2) );
}

/* ***************  CAIRO OVERLOADS *************** */

void
cairo_line_to(draw_context const& dc, coordinate_2D const& point)
{
    cairo_line_to(
            dc,
            point.x.value(dc.width),
            point.y.value(dc.height)
            );
}

void
cairo_move_to(draw_context const& dc, coordinate_2D const& point)
{
    cairo_move_to(
            dc,
            point.x.value(dc.width),
            point.y.value(dc.height)
            );
}


//___________________________________________________________________ VIEW_LIMIT


bool operator == (view_limit_t const& lhs, view_limit_t const& rhs)
{
    return lhs.min == rhs.min && lhs.max == rhs.max;
}


bool operator != (view_limit_t const& lhs, view_limit_t const& rhs)
{
    return !(lhs == rhs);
}


double
view_limit_t:: pixel_to_data (double y) const
{
    if (a != 0.)
        return /* x= */ (y - b)/a;
    else
        return 0.;
}


double
view_limit_t:: scale (double datum) const
{
    return a*datum + b;
}


view_limit_t:: view_limit_t ()
  : min(autom)
  , max(autom)
{ }


view_limit_t:: view_limit_t(view_limit_types_t autom_limit)
  : min(autom_limit)
  , max(autom_limit)
{ }


//________________________________________________________________________ COLOR


color_t:: color_t()
    : color_t(0,0,0, 1)
{ }

color_t:: color_t(uint32_t hex)
    : color_t(hex, 1.)
{ }

color_t:: color_t(uint32_t hex, float A)
{
    r = ((hex >> 16) & 0xFF) / 255.;
    g = ((hex >>  8) & 0xFF) / 255.;
    b = ((hex >>  0) & 0xFF) / 255.;
    a = std::max(0.f, A); a = std::min(A, 1.f);
}

color_t:: color_t(float R, float G, float B)
    : color_t(R, G, B, 1.)
{ }

color_t:: color_t(float R, float G, float B, float A)
{
    r = std::max(0.f, R); r = std::min(R, 1.f);
    g = std::max(0.f, G); g = std::min(G, 1.f);
    b = std::max(0.f, B); b = std::min(B, 1.f);
    a = std::max(0.f, A); a = std::min(A, 1.f);
}

bool
operator == (color_t const& lhs, color_t const& rhs)
{
    return lhs.r == rhs.r
        && lhs.g == rhs.g
        && lhs.b == rhs.b
        && lhs.a == rhs.a;
}

bool
operator != (color_t const& lhs, color_t const& rhs)
{
    return !(lhs == rhs);
}
