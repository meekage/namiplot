/*________________________________ PLOT_BASE _________________________________*/


class plot_base_t
{

/* CALLABLE INTERFACE FOR FINAL USERS */
public:
    void render(cairo_surface_t * surface) const;


/* CALLABLE INTERFACE FOR PLOT CHILDS */
protected:

protected:
    static void draw(draw_context & dc, plot_base_t const& pl);


/* INTERNAL INTERFACE FOR FINAL USERS */
public:
    virtual ~plot_base_t();


/* INTERNAL INTERFACE FOR PLOT CHILDS */
protected:
public: // FIXME
    void register_parameter();
protected:

    /* Instantiate this function as appropriate.
     * Called by draw(dc, plot) */
    virtual void draw(draw_context & cairo) const = 0;

};

