#ifndef PLOT_CORE_HH
#define PLOT_CORE_HH

#include "parameter_types.hh"
#include "utilities.hh"

#include <cairo/cairo.h>

#include <any>
#include <iterator>
#include <string>
#include <type_traits>
#include <vector>

class plot;
using plot_ptr = std::shared_ptr<plot>;

//_________________________________________________________________________ PLOT

class plot
{
protected:
    /* *** DRAW FUNCTIONS ***
     *
     * Draw functions are internal to namiplot.
     *
     * The helper function draw(cairo_t * context, plot * pl) grants access to
     * protected * virtual member function draw() in derived classes. Thus, draw
     * is hidden from users * but accessible for derived classes.
     */

    // These are the functions to call from other plots.
    static void static_draw (draw_context & dc, plot & pl);

    // Instantiate this functions as appropriate.
    // Do not call them directly.
    // Call 'draw(dc, plot)' instead.
    virtual void draw(draw_context & dc) = 0;

    // Returns "needs redrawing?"
    public: virtual bool rebuild_caches(draw_context & dc);

protected: /* Common parameter initialization functions. */
    inline void init() { };

    // TODO: Concepts for _namiplot_keyword_argument_t_
    //       T is decltype(_namiplot_keyword_argument_t_::operator=)
    template<typename T, typename ... param_args_t>
    void init(T const& arg, param_args_t ... param_args)
    {
        *std::any_cast<
            typename std::remove_const< typename std::remove_reference<
                typename T::second_type
            >::type >::type * >
            ( parameters[arg.first] ) = arg.second;
        plot::init(param_args...);
    }

protected: // FIXME: make private
    // std::any::value_type is pointer to parameter_type
    using parameters_t = std::map<std::type_index, std::any>;
    parameters_t parameters;

public:
    // TODO: SFINAE/Concepts: T is instance of _namiplot_keyword_argument_t_
    template<typename T>
    typename T::value_type * parameter(T const&);

public:
    coordinate_1D min_width  {0._px};
    coordinate_1D min_height {0._px};

public:
    /* User accessible wrapper for the draw functions */
    void update(cairo_surface_t * surface);

    virtual ~plot() {};
};

// The type of the argument is used to deduce the output type.
template<typename T>
typename T::value_type *
plot:: parameter(T const&)
{
    //FIXME: C++20 contains()
    auto parm = parameters.find(typeid(T));
    if (parm == parameters.end())
        return {};
    auto s = parm->second.type().name();
    auto t = typeid(typename T::value_type *).name();
    std::any & sec = parm->second;
    color_t * col = std::any_cast<color_t *>(sec);
    color_t * col2 = std::any_cast<typename T::value_type *>(sec);
    return std::any_cast<typename T::value_type *>(sec);
}



//_______________________________________________________________________ NOPLOT

class noplot : public plot
{
protected:
    void draw(draw_context &) override { }
    bool rebuild_caches(draw_context &) override { return false; }
public:
    noplot() : plot() { }
};

//________________________________________________________________________ INSET


class inset : public plot
{
protected: /* Common virtual functions. */
    void draw(draw_context & dc) override;

public: /* Plot-specific constructors */
    template<typename Plot, typename ... param_args_t>
    inset(Plot const& inset_pl, param_args_t ... param_args)
      : inset( plot_ptr(new Plot(inset_pl)) )
    { }

    template<typename Plot, typename ... param_args_t>
    inset(std::shared_ptr<Plot> const& inset_pl, param_args_t ... param_args)
      : inset_plot(inset_pl)
    {
        REGISTER_PARAMETER(border_color);
        REGISTER_PARAMETER(border_width);
        REGISTER_PARAMETER(margins);
        plot::init(param_args...);
    }

public: /* Public members */
    color_t border_color = black;
    float border_width = 0.;
    margins_t margins {25_pct, 25_pct, 25_pct, 25_pct};

    plot_ptr inset_plot;
};


//____________________________________________________________________ MULTIPLOT



//____________________________________________________________________ DATA_PLOT

// TODO: Use RCU (Read-Copy Update) for data
template<typename D>
class data_plot : public plot
{
protected: /* Common virtual functions. */
    //bool rebuild_caches(draw_context & dc) override;

public:
    // TODO: Use RCU (Read-Copy Update)
    template<typename ... param_args_t>
    data_plot(std::shared_ptr<D> data_arg, param_args_t ... param_args)
    : data_ptr(data_arg)
    {
        REGISTER_PARAMETER(scale);
        plot::init(param_args...);
    }

    /*
    template<typename ... param_args_t>
    data_plot(D const& data_val, param_args_t ... param_args)
    : data_ptr(std::make_shared<D>(data_val))
    {
        REGISTER_PARAMETER(scale);
        plot::init(param_args...);
    }
    */

public:
    // FIXME Name of _variable_ data_ptr too similiar to name of _type_ plot_ptr.
    std::shared_ptr<D> data_ptr;
    //std::mutex *       line_data_mtx;
    scale_t scale;
};

#endif
