void plot_to_PNG(
        plot_base_t const& p,
        std::filesystem::path output,
        int png_width,
        int png_height
        );

