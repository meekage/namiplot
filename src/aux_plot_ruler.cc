#include "namiplot"


double floor125 (double x)
{
    using namespace std;
    if (x == 0) return 0;

    double ret = pow(10, floor(log10(abs(x))) );
    if      (5*ret <= x)
        ret *= 5;
    else if (2*ret <= x)
        ret *= 2;
    return (x < 0 ? -1 : 1) * ret;
}


std::vector<double> ruler_ticks(
        double data_min,
        double data_max,
        unsigned n_ticks
        )
{
    std::vector<double> ret;

    double data_diff = data_max - data_min;
    double interval  = floor125(data_diff / (n_ticks - 1));

    double min_tick = interval * int(data_min / interval);
    for (int i = 0; (min_tick + i * interval) <= data_max; i++)
        ret.push_back(min_tick + i * interval);

    return ret;
}



/*________________________________ PLOT_RULER ________________________________*/


void
plot_ruler_t:: draw(draw_context & cairo) const
{
    auto view_limits_ptr =
        dynamic_cast<PARAMETER_TYPE(view_limits) *>(plot_with_limits.get());

    if (!view_limits_ptr)
        throw std::runtime_error("Namiplot bug");

    view_limit_2D_t const& limits = view_limits_ptr->value();
    std::vector<double> ticks_x = ruler_ticks(
            limits.x.pixel_to_data(0),
            limits.x.pixel_to_data(cairo.width),
            5);
    std::vector<double> ticks_y = ruler_ticks(
            limits.y.pixel_to_data(0),
            limits.y.pixel_to_data(cairo.height),
            5);

    for (auto tick : ticks_x)
    {
        cairo_move_to(cairo,
                limits.x.scale(tick),
                cairo.height);
        cairo_line_to(cairo,
                limits.x.scale(tick),
                cairo.height - 4.5);
    }
    for (auto tick : ticks_y)
    {
        cairo_move_to(cairo,
                0,
                cairo.height - limits.y.scale(tick));
        cairo_line_to(cairo,
                4.5,
                cairo.height - limits.y.scale(tick));
    }

    cairo_set_source_rgb(cairo, black);
    cairo_set_line_width(cairo, 1);
    cairo_stroke(cairo);
}



