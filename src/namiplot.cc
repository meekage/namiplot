#include "namiplot"

#include <armadillo>

#include <cmath>
#include <mutex>
#include <unistd.h>


template <typename T>
std::ostream & operator << (std::ostream & out, data_t<T> const& data)
{
    for (auto d : data)
        out << d << ' ';

    return out << std::endl;
}
template <typename T>
std::ostream & operator << (std::ostream & out, std::vector<T> const& data)
{
    for (auto d : data)
        out << d << ' ';

    return out << std::endl;
}

int
main()
{
    // ERror auto ruler = ruler_ticks(357, 908375, 4);
    auto ruler = ruler_ticks(200, 500000, 4);
    std::cout << "DEBUG: ruler ticks " << ruler << std::endl;


    arma::arma_rng::set_seed_random();

    arma::mat data
      { {14, 11, 16, 15, 16, 17 }
      , {24, 20, 18, 22, 26, 22 }
      , {44, 44, 36, 23, 50, 44 }
      };
    data = arma::randn(7,6);

    arma::mat row { {14, 12, 16, 17, 16, 17 } };


    // FIXME: Esto no funciona, pero si funciona en armadillo... <stashed>
    // Hay algo raro con la version. Mejor hacer stash pop otravez
    //plot_line_t line(arma::randu(2,6)
    plot_line_t line2(row, data
            , width=3
            //, width={12.,4.,1.}
            //, color={matred, matblue}
            , color=matred
            );

    std::vector<char> x = {10, 20, 30, 40, 50, 60};

    plot_line_t line(x, &data
            , width=1.3
            , color=darkcyan
            );

    plot_frame_t frame(line);




    plot_to_PNG(frame, "prueba.png", 800, 600);
    X11_window output1(frame, 60, 800,600);

    while(output1.is_active())
    {
        ++data(2,2);
        usleep(25000);
    }

    return 0;
}

