// FIXME: Protection against assignment of nullptr in all Namidata specializations.
// FIXME: Test for mutexes
template <Namidata_vector DATA>
class data_t<DATA>
{
private:
    // FIXME: Use const in shared_ptr and use observer_ptr.
    //        Then get_ptr() should be casted to non-const.
    using user_ptr_t = std::variant<std::shared_ptr<DATA>, DATA const*>;
    user_ptr_t user_ptr;

    DATA const* ptr() const;

public:
    using data_type = DATA;

    data_t();
    data_t(data_t const& copy);

    data_t & operator = (data_t const& copy);


    data_t(DATA const& copy_data);
    data_t(std::shared_ptr<DATA> const& shared_data);
    data_t(DATA const* observable_data);

    data_t & operator = (DATA const& copy_data);
    data_t & operator = (std::shared_ptr<DATA> const& shared_data);
    data_t & operator = (DATA const* observable_data);

public:

    size_t size() const;
    size_t cols() const;
    size_t rows() const;
    bool   empty() const;


    std::shared_ptr<DATA> get_ptr() const;


    std::experimental::observer_ptr<std::mutex> mutex= nullptr;

public: /* Iterators */

    class const_row_iterator;

    DATA::const_iterator begin() const;
    DATA::const_iterator end  () const;

    const_row_iterator begin_row() const;
    const_row_iterator end_row  () const;
};


/*_________________________________ ITERATORS ________________________________*/


template <Namidata_vector DATA>
class data_t<DATA>:: const_row_iterator
{
    DATA const * value = nullptr;

public:

    using difference_type   = DATA::difference_type;
    using value_type        = DATA;
    using pointer           = const DATA *;
    using reference         = const DATA &;
    using iterator_category = std::input_iterator_tag;


    reference operator * () const
    { return *value; }

    pointer operator -> () const
    { return value; }


    const_row_iterator operator ++ ()
    { value = nullptr; return *this; }

    const_row_iterator operator ++ (int)
    { const_row_iterator tmp = *this; value = nullptr; return tmp; }


    friend bool operator == (const_row_iterator const& lhs,
                             const_row_iterator const& rhs)
    { return lhs.value == rhs.value; }

    friend bool operator != (const_row_iterator const& lhs,
                             const_row_iterator const& rhs)
    { return !(lhs == rhs); }


    const_row_iterator() = default;

    const_row_iterator(DATA const& data)
      : value(&data)
    {}
};


/*________________________________ DEFINITIONS _______________________________*/


template <Namidata_vector DATA>
size_t
data_t<DATA>:: cols() const
{
    return ptr()->size();
}


template <Namidata_vector DATA>
data_t<DATA>:: data_t()
  : user_ptr(std::make_shared<DATA>( DATA{} ))
{
}


template <Namidata_vector DATA>
data_t<DATA>:: data_t(DATA const& copy_data)
  : user_ptr(std::make_shared<DATA>(copy_data))
{
}


template <Namidata_vector DATA>
data_t<DATA>:: data_t(std::shared_ptr<DATA> const& shared_data)
  : user_ptr(shared_data)
{
}


template <Namidata_vector DATA>
data_t<DATA>:: data_t(DATA const* observable_data)
  : user_ptr(observable_data)
{
}


template <Namidata_vector DATA>
data_t<DATA>:: data_t(data_t const& copy)
  : user_ptr     {copy.user_ptr},
    mutex        {copy.mutex}
{
}


template <Namidata_vector DATA>
bool
data_t<DATA>:: empty() const
{
    return ptr()->empty();
}


template <Namidata_vector DATA>
std::shared_ptr<DATA>
data_t<DATA>:: get_ptr() const
{
    if (user_ptr.index() == 0)
        return std::get<0>(user_ptr);
    else
        return {};
}


template <Namidata_vector DATA>
data_t<DATA> &
data_t<DATA>:: operator = (DATA const& copy_data)
{
    user_ptr = std::make_shared<DATA>(copy_data);
    return *this;
}


template <Namidata_vector DATA>
data_t<DATA> &
data_t<DATA>:: operator = (std::shared_ptr<DATA> const& shared_data)
{
    user_ptr = shared_data;
    return *this;
}


template <Namidata_vector DATA>
data_t<DATA> &
data_t<DATA>:: operator = (DATA const* observable_data)
{
    user_ptr = observable_data;
    return *this;
}


template <Namidata_vector DATA>
data_t<DATA> &
data_t<DATA>:: operator = (data_t const& copy)
{
    user_ptr = copy.user_ptr;
    mutex = copy.mutex;
    return *this;
}


template <Namidata_vector DATA>
DATA const*
data_t<DATA>:: ptr() const
{
    if (user_ptr.index() == 0)
        return std::get<0>(user_ptr).get();
    else
        return std::get<1>(user_ptr);
}


template <Namidata_vector DATA>
size_t
data_t<DATA>:: rows() const
{
    return ptr()->size() > 0;
}


template <Namidata_vector DATA>
size_t
data_t<DATA>:: size() const
{
    return ptr()->size();
}


/** Iterator related **/


template <Namidata_vector DATA>
DATA::const_iterator
data_t<DATA>:: begin() const
{
    return ptr()->begin();
}

template <Namidata_vector DATA>
DATA::const_iterator
data_t<DATA>:: end() const
{
    return ptr()->end();
}


template <Namidata_vector DATA>
data_t<DATA>::const_row_iterator
data_t<DATA>:: begin_row() const
{
    return const_row_iterator(*ptr());
}

template <Namidata_vector DATA>
data_t<DATA>::const_row_iterator
data_t<DATA>:: end_row() const
{
    return const_row_iterator();
}

