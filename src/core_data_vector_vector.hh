template <Namidata_vector_vector DATA>
class data_t<DATA>
{
public:

    /* TODO: For now, assume column-major ordering
     *enum ordering_t {
     *    row_major,
     *    column_major,
     *};
     *ordering_t ordering = column_major;
     */

private:
    // FIXME: Use const in shared_ptr and use observer_ptr.
    using user_ptr_t = std::variant<std::shared_ptr<DATA>, DATA const*>;
    user_ptr_t user_ptr;

    DATA const* ptr() const;

public:
    using data_type = DATA;

    data_t();
    data_t(data_t const& copy);

    data_t & operator = (data_t const& copy);


    data_t(DATA const& copy_data);
    data_t(std::shared_ptr<DATA> const& shared_data);
    data_t(DATA const* observable_data);

    data_t & operator = (DATA const& copy_data);
    data_t & operator = (std::shared_ptr<DATA> const& shared_data);
    data_t & operator = (DATA const* observable_data);

private:
    static void assert_row_consistency(DATA const* p);

public:

    size_t size() const;
    size_t cols() const;
    size_t rows() const;
    bool   empty() const;


    std::shared_ptr<DATA> get_ptr() const;


    std::experimental::observer_ptr<std::mutex> mutex = nullptr;

public: /* Iterators */

    class const_element_iterator;

    const_element_iterator begin() const;
    const_element_iterator end()   const;

    DATA::const_iterator begin_row() const;
    DATA::const_iterator end_row()   const;
};


/*_________________________________ ITERATORS ________________________________*/


template <Namidata_vector_vector DATA>
class data_t<DATA>:: const_element_iterator
{
    DATA::const_iterator row_iterator;
    std::optional<typename DATA::value_type::const_iterator> element_iterator;
    DATA::const_iterator end_iterator;

public:
    using difference_type   = DATA::value_type::const_iterator::difference_type;
    using value_type        = DATA::value_type::const_iterator::value_type;
    using pointer           = DATA::value_type::const_iterator::pointer;
    using reference         = DATA::value_type::const_iterator::reference;
    using iterator_category =
              DATA::value_type::const_iterator::iterator_category;

    value_type operator * () const
    {
        if (element_iterator)
            return element_iterator->operator *();
        else
            throw std::out_of_range("Cannot dereference past-the-end iterator");
    }

    pointer operator -> () const
    {
        if (element_iterator)
            return element_iterator->operator->();
        else
            throw std::out_of_range("Cannot dereference past-the-end iterator");
    }

    const_element_iterator operator ++ ()
    {
        if (element_iterator)
        {
            if (++*element_iterator == row_iterator->end())
            {
                if (++row_iterator != end_iterator)
                    element_iterator = row_iterator->begin();
                else
                    element_iterator.reset();
            }
        }
        return *this;
    }

    const_element_iterator operator ++ (int)
    {
        const_element_iterator tmp = *this;
        operator ++ ();
        return tmp;
    }

    friend bool operator == (const_element_iterator const& lhs,
                             const_element_iterator const& rhs)
    {
        if (!lhs.element_iterator && !rhs.element_iterator)
            return true;
        else
            return lhs.row_iterator == rhs.row_iterator
                && lhs.element_iterator == rhs.element_iterator;
    }

    friend bool operator != (const_element_iterator const& lhs,
                             const_element_iterator const& rhs)
    { return !(lhs == rhs); }

    const_element_iterator() = default;
    const_element_iterator(DATA const& data)
        : row_iterator(data.begin()),
        end_iterator(data.end())
    {
        if (!data.empty())
            element_iterator = row_iterator->begin();
    }
};


/*________________________________ DEFINITIONS _______________________________*/


template <Namidata_vector_vector DATA>
void
data_t<DATA>:: assert_row_consistency(DATA const* p)
{
    if (!p)
        throw std::runtime_error("Namiplot error");

    if (!p->empty())
    {
        size_t col_size = p->operator[](0).size();
        for (auto const& row : *p)
            if (row.size() != col_size)
                throw std::range_error("Row length mismatch in container");
    }
}


template <Namidata_vector_vector DATA>
size_t
data_t<DATA>:: cols() const
{
    return ptr()->size() ? ptr()->operator[](0).size() : 0;
}


template <Namidata_vector_vector DATA>
data_t<DATA>:: data_t()
  : user_ptr(std::make_shared<DATA>( DATA{} ))
{
}


template <Namidata_vector_vector DATA>
data_t<DATA>:: data_t(DATA const& copy_data)
  : user_ptr(std::make_shared<DATA>(copy_data))
{
    assert_row_consistency(&copy_data);
}


template <Namidata_vector_vector DATA>
data_t<DATA>:: data_t(std::shared_ptr<DATA> const& shared_data)
  : user_ptr(shared_data)
{
    assert_row_consistency(shared_data.get());
}


template <Namidata_vector_vector DATA>
data_t<DATA>:: data_t(DATA const* observable_data)
  : user_ptr(observable_data)
{
    assert_row_consistency(observable_data);
}


template <Namidata_vector_vector DATA>
data_t<DATA>:: data_t(data_t const& copy)
  : user_ptr     {copy.user_ptr},
    mutex        {copy.mutex}
{
}


template <Namidata_vector_vector DATA>
bool
data_t<DATA>:: empty() const
{
    return ptr()->empty() || ptr()->operator[](0).empty();
}


template <Namidata_vector_vector DATA>
std::shared_ptr<DATA>
data_t<DATA>:: get_ptr() const
{
    if (user_ptr.index() == 0)
        return std::get<0>(user_ptr);
    else
        return {};
}


template <Namidata_vector_vector DATA>
data_t<DATA> &
data_t<DATA>:: operator = (DATA const& copy_data)
{
    assert_row_consistency(&copy_data);
    user_ptr = std::make_shared<DATA>(copy_data);
    return *this;
}


template <Namidata_vector_vector DATA>
data_t<DATA> &
data_t<DATA>:: operator = (std::shared_ptr<DATA> const& shared_data)
{
    assert_row_consistency(shared_data.get());
    user_ptr = shared_data;
    return *this;
}


template <Namidata_vector_vector DATA>
data_t<DATA> &
data_t<DATA>:: operator = (DATA const* observable_data)
{
    assert_row_consistency(observable_data);
    user_ptr = observable_data;
    return *this;
}


template <Namidata_vector_vector DATA>
data_t<DATA> &
data_t<DATA>:: operator = (data_t const& copy)
{
    user_ptr = copy.user_ptr;
    mutex = copy.mutex;
    return *this;
}


template <Namidata_vector_vector DATA>
DATA const*
data_t<DATA>:: ptr() const
{
    if (user_ptr.index() == 0)
        // FIXME: Remove this ifelse
        if (std::get<0>(user_ptr))
            return std::get<0>(user_ptr).get();
        else
            throw std::runtime_error("Namiplot BUG: nullptr in data_vector");
    else
        return std::get<1>(user_ptr);
}


template <Namidata_vector_vector DATA>
size_t
data_t<DATA>:: rows() const
{
    return ptr()->size();
}


template <Namidata_vector_vector DATA>
size_t
data_t<DATA>:: size() const
{
    size_t elements = ptr()->size();
    if (elements)
        elements *= ptr()->operator[](0).size();
    return elements;
}


/** Iterator related **/


template <Namidata_vector_vector DATA>
data_t<DATA>::const_element_iterator
data_t<DATA>:: begin() const
{
    return const_element_iterator(*ptr());
}

template <Namidata_vector_vector DATA>
data_t<DATA>::const_element_iterator
data_t<DATA>:: end() const
{
    return const_element_iterator();
}


template <Namidata_vector_vector DATA>
DATA::const_iterator
data_t<DATA>:: begin_row() const
{
    return ptr()->begin();
}

template <Namidata_vector_vector DATA>
DATA::const_iterator
data_t<DATA>:: end_row() const
{
    return ptr()->end();
}

