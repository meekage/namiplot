#ifndef IMPRINT_HH
#define IMPRINT_HH

#include <cairo/cairo.h>
#include <array>
#include <filesystem>

//__________________________________________________________________ PDF_PATTERN

enum symbol_preset_t {
    plus     = 0,
    circle   = 1,
    asterisk = 2,
    point    = 3,
    cross    = 4,
    square   = 5,
    diamond  = 6,
    /*
    tri_up,
    tri_down,
    tri_left,
    tri_right,
    star5,
    star6,
    hexagon,
    clover,
    heart,
    // TODO: See matplotlib.markers for many more imprint presets.
    */
};

// Symbols are rendered in a square with corners (-1,-1) and (1,1)
extern
std::array<void (*)(cairo_t *), 7> const symbol_presets;


/*
PDF_pattern_tfd(imprint_preset_t )
{
    // Cairo context
    cairo_rectangle_t rect {-0.5, -0.5, 1, 1}; // Unit rectangle centered in 0.
    cairo_surface_t * surface =
        cairo_recording_surface_create( CAIRO_CONTENT_COLOR_ALPHA, &rect );
    cairo_t * cairo = cairo_create( surface );


    cairo_arc(cairo, 0, 0, 1., 0., 2.*pi);

    path = cairo_copy_path(cairo);
    cairo_destroy(cairo);
    cairo_surface_destroy(surface);
}
*/



/** PDF_pattern_t
 * Imports a PDF from a file and stores it in a cairo pattern.
 * Bounding box top left corner is at origin and has width() and height().
 */
class PDF_pattern_t
{
private:
    cairo_pattern_t * pattern = nullptr;
    double w = 0;
    double h = 0;

public:
    PDF_pattern_t() = default;
    PDF_pattern_t(PDF_pattern_t const& other);
    PDF_pattern_t(PDF_pattern_t     && other);
    PDF_pattern_t(std::filesystem::path filename);

    PDF_pattern_t & operator = (PDF_pattern_t const& other);

    ~PDF_pattern_t();

    void cairo_mask(cairo_t * dc);
    double width()  { return w; }
    double height() { return h; }
};


#endif
