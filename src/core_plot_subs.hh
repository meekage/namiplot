/*________________________________ SUBPLOTS __________________________________*/

class plot_subs_t : public plot_multi_t,
PARAMETER_INSERT(n_rows)
{
PARAMETER_SETUP(n_rows, 2);

private:
    plot_subs_t() :
        PARAMETER_CONSTRUCT(n_rows)
    {}

    template <typename ... Args>
    plot_subs_t(Assigned_parameter const& keyword_argument, Args ... args)
      : plot_subs_t(args...)
    {
        keyword_invoked(keyword_argument);
    }


public:

    // TODO: Use concepts on P.
    template<typename P,
             typename ... Args,
             std::enable_if_t<std::is_base_of<plot_base_t,P>::value>* = nullptr
             >
    // Without const it does not display anything!
    //subplots(P & plot_arg, param_args_t ... param_args)
    plot_subs_t(P const& plot_arg, Args ... args)
      : plot_subs_t(args ... )
    {
        push_top(plot_arg);
    }


    template <typename ... Args>
    plot_subs_t(plot_ptr const& plot_arg, Args ... args)
      : plot_subs_t(args ... )
    {
        push_top(plot_arg);
    }


protected:
    void draw(draw_context & cairo) const override;


public: /* Public members */
    unsigned int n_cols() const;

    template<typename D>
    void   push_back   (D        const& plot_obj );
    void   push_back   (plot_ptr const& sptr     );

    // TODO: Llevar a parametros
    std::vector<double> ratios_rows;
    std::vector<double> ratios_cols;
};


/* ********** TEMPLATE FUNCTIONS ********** */

template<typename D>
void
plot_subs_t:: push_back( D const& plot_obj )
{
    push_bot(plot_obj);
}

