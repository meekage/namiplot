#ifndef FRAME_HH
#define FRAME_HH

#include "plot_core.hh"


//________________________________________________________________________ TICKS

class ticks : public plot
{
protected: /* Common virtual functions. */
    void draw(draw_context & dc) override;
    bool rebuild_caches(draw_context & dc) override;

public: /* Plot-specific constructors */
    template<typename ... param_args_t>
    ticks(param_args_t ... param_args)
    {
        REGISTER_PARAMETER(color);
        REGISTER_PARAMETER(margins);
        REGISTER_PARAMETER(width);
        plot::init(param_args...);
    }

public:
    color_t color = black;
    float width = 1.;
    margins_t margins { 0_pct, 0_pct, 0_pct, 0_pct};
};


//________________________________________________________________________ FRAME


class frame : public inset
{
protected: /* Common virtual functions. */
    void draw(draw_context & dc) override;
    bool rebuild_caches(draw_context & dc) override;

public: /* Plot-specific constructors */
    template<typename Plot, typename ... param_args_t>
    frame(Plot inset_contents, param_args_t ... param_args)
      : inset(inset_contents)
    {
        REGISTER_PARAMETER(background);
        plot::init(param_args...);
    }

public:
    using frame_decorations_ptr = std::shared_ptr<subplots>;
    sides<frame_decorations_ptr> decorations = {
        frame_decorations_ptr(new subplots()) ,
        frame_decorations_ptr(new subplots()) ,
        frame_decorations_ptr(new subplots()) ,
        frame_decorations_ptr(new subplots()) };

    ticks prueba_ticks;

private:
    void draw(draw_context & dc,
              frame_decorations_ptr decoration,
              margins_t inset_margins);

    void rebuild_caches(draw_context & dc,
                        frame_decorations_ptr decoration,
                        coordinate_1D & margin,
                        double span);

public: /*** Parameters ***/
    color_t background = transparent;

};

#endif
