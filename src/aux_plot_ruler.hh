
std::vector<double> ruler_ticks(
        double data_min,
        double data_max,
        unsigned n_ticks
        // scheme_t scheme
        );


/*________________________________ PLOT_RULER ________________________________*/


class plot_ruler_t : public plot_base_t,
PARAMETER_INSERT(width)
{
PARAMETER_SETUP(width, 1.);

private:
    plot_ruler_t() :
        PARAMETER_CONSTRUCT(width)
    {}

    template <typename ... Args>
    plot_ruler_t(Assigned_parameter const& keyword_argument, Args ... args)
      : plot_ruler_t(args...)
    {
        keyword_invoked(keyword_argument);
    }


public:

    template<typename ... Args>
    plot_ruler_t(plot_ptr reference_plot, Args ... args)
      : plot_ruler_t(args...)
    {
        if (!reference_plot)
            throw std::runtime_error("DEBUG: plot_ruler_t did not find "
                    "view_limits in reference plot ");

        plot_with_limits = reference_plot;
    }


protected:
    void draw(draw_context & cairo) const override;


private:
    plot_ptr plot_with_limits;
};





