#include "namiplot"

/*________________________________ PLOT_BASE _________________________________*/


void
plot_base_t:: draw(draw_context & cairo, plot_base_t const& pl)
{
    pl.draw(cairo);
}


plot_base_t:: ~plot_base_t()
{
}


void
plot_base_t:: register_parameter()
{
}


void
plot_base_t:: render(cairo_surface_t * surface) const
{
    draw_context cairo {nullptr, 0, 0};
    switch (cairo_surface_get_type(surface))
    {
      case CAIRO_SURFACE_TYPE_XLIB:
        cairo.width  = cairo_xlib_surface_get_width(surface);
        cairo.height = cairo_xlib_surface_get_height(surface);
        cairo.cairo  = cairo_create(surface);
        break;
      case CAIRO_SURFACE_TYPE_IMAGE:
        cairo.width  = cairo_image_surface_get_width(surface);
        cairo.height = cairo_image_surface_get_height(surface);
        cairo.cairo  = cairo_create(surface);
        break;
      default:
        throw std::runtime_error("__FILE__:__LINE__: "
                "Cairo surface type not supported.");
    }

    { cairo_group cg(cairo);
        cairo_set_source_rgb(cairo, white);
        cairo_paint(cairo);
        draw(cairo, *this);
    }

    cairo_destroy(cairo);
}

