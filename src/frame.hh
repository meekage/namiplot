/* Contains an inset plot with several layers that are handled internally:
 *  - Layer 0: User plot (which can be multiplot).
 *  - Layer 1: Ruler. Created and destroyed at draw().
 */
class plot_frame_t : public plot_base_t
{

private:
    plot_frame_t()
      : framed_multi( std::make_shared<plot_multi_t>() )
      , framed_inset( framed_multi , margins={30_pct, 10_pct} )
    {}

    template <typename ... Args>
    plot_frame_t(Assigned_parameter const& keyword_argument, Args ... args)
      : plot_frame_t(args...)
    {
        keyword_invoked(keyword_argument);
    }


public:


    template <typename P, typename ... Args>
        requires std::is_base_of<plot_base_t, P>::value
    plot_frame_t (P const& framed_pl, Args ... args)
      : plot_frame_t(args...)
    {
        framed_multi->push_bot(framed_pl);
    }


    template <typename ... Args>
    plot_frame_t(plot_ptr framed_pl, Args ... args)
      : plot_frame_t(args...)
    {
        framed_multi->push_bot(framed_pl);
    }


protected:
    void draw(draw_context & cairo) const override;


private:
    std::shared_ptr<plot_multi_t> framed_multi;
    plot_inset_t framed_inset;
public:
    plot_ptr plot() const
    {
        return framed_multi->at(0);
    }

};
