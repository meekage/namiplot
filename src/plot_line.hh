/*________________________________ PLOT_LINE _________________________________*/


template <Namidata DATA_X, Namidata DATA_Y>
class plot_line_t : public plot_base_t,
PARAMETER_INSERT(color),
PARAMETER_INSERT(view_limits),
PARAMETER_INSERT(pattern),
PARAMETER_INSERT(width)
{
PARAMETER_SETUP(color,       black);
PARAMETER_SETUP(view_limits, autom);
PARAMETER_SETUP(pattern,     solid);
PARAMETER_SETUP(width,       1.);

private:
    plot_line_t() :
        PARAMETER_CONSTRUCT(color),
        PARAMETER_CONSTRUCT(view_limits),
        PARAMETER_CONSTRUCT(pattern),
        PARAMETER_CONSTRUCT(width)
    {}

    template <typename ... Args>
    plot_line_t(Assigned_parameter const& keyword_argument, Args ... args)
      : plot_line_t(args...)
    {
        keyword_invoked(keyword_argument);
    }


public:

    template <Namidata_argument D, typename ... Args>
    plot_line_t(D data_arg, Args ... args)
      : plot_line_t(args...)
    {
        Y = data_arg;
    }

    template <Namidata_argument D, typename ... Args>
    plot_line_t(D data_arg, std::mutex & mtx, Args ... args)
      : plot_line_t(args...)
    {
        Y = data_arg;
        Y.mutex = std::experimental::make_observer(&mtx);
    }

    template <Namidata_argument DX, Namidata_argument DY, typename ... Args>
    plot_line_t(DX x, DY y, Args ... args)
      : plot_line_t(args...)
    {
        X = x;
        Y = y;
    }

    template <Namidata_argument DX, Namidata_argument DY, typename ... Args>
    plot_line_t(DX x, DY y, std::mutex & mtx, Args ... args)
      : plot_line_t(args...)
    {
        X = x;
        Y = y;
        X.mutex = std::experimental::make_observer(&mtx);
        Y.mutex = std::experimental::make_observer(&mtx);
    }


protected:
    void draw(draw_context & cairo) const override;


public:
    data_t<DATA_X> X;
    data_t<DATA_Y> Y;

};


// FIXME: Wait for proposal P1021 to be implemented before using line.
//template <Namidata DATA_X>
//using line = plot_line_t<DATA_X>;


template <Namidata_argument D, typename ... Args>
plot_line_t(D data_arg, Args ... args)
    -> plot_line_t< typename decltype(data_t(data_arg))::data_type
                  , typename decltype(data_t(data_arg))::data_type >;

template <Namidata_argument D, typename ... Args>
plot_line_t(D data_arg, std::mutex & mtx, Args ... args)
    -> plot_line_t< typename decltype(data_t(data_arg))::data_type
                  , typename decltype(data_t(data_arg))::data_type >;

template <Namidata_argument DX, Namidata_argument DY, typename ... Args>
plot_line_t(DX x, DY y, Args ... args)
    -> plot_line_t< typename decltype(data_t(x))::data_type
                  , typename decltype(data_t(y))::data_type >;


template <Namidata_argument DX, Namidata_argument DY, typename ... Args>
plot_line_t(DX x, DY y, std::mutex & mtx, Args ... args)
    -> plot_line_t< typename decltype(data_t(x))::data_type
                  , typename decltype(data_t(y))::data_type >;


/*___ FUNCTIONS __________________ PLOT_LINE _________________________________*/



template <Namidata DATA_X, Namidata DATA_Y>
void
plot_line_t<DATA_X, DATA_Y>:: draw(draw_context & cairo) const
{
    if (!X.empty())
    {
        if (X.rows() != 1)
            throw std::range_error("X data must be single row");
        if (X.size() != Y.cols())
        {
            throw std::range_error(
                    "plot_line_t: X and Y have incompatible dimensions");
        }
        PARAM(view_limits).x.calculate_view_limits(X, cairo.width);
    }
    else
    {
        data_t x_borders =
            std::vector<double>{1., static_cast<double>(Y.cols())};
        PARAM(view_limits).x.calculate_view_limits(x_borders, cairo.width);
    }
    PARAM(view_limits).y.calculate_view_limits(Y, cairo.height);


    cairo_set_source_rgb(cairo, PARAM(color));
    cairo_set_line_width(cairo, PARAM(width));

    std::vector<double> line_dash = PARAM(pattern);
    std::for_each(line_dash.begin(), line_dash.end(),
                  [this](double & p){ p *= width; } );
    cairo_set_dash(cairo, line_dash.data(), line_dash.size(), 0.);

    view_limit_t const& view_limit_x = PARAM(view_limits).x;
    auto         const& view_limit_y = PARAM(view_limits).y;
    if (!X.empty())
    {
        for (auto y_row : rows(Y))
        {
            for (auto x_it = X.begin(); auto y : y_row)
            {
                cairo_line_to(cairo,
                        view_limit_x.scale(*x_it),
                        cairo.height - view_limit_y.scale(y) );
                ++x_it;
            }
            cairo_stroke(cairo);
        }
    }
    else
    {
        for (auto y_row : rows(Y))
        {
            for (double i = 1.; auto y : y_row)
            {
                cairo_line_to(cairo,
                        view_limit_x.scale(i),
                        cairo.height - view_limit_y.scale(y) );
                i++;
            }
            cairo_stroke(cairo);
        }
    }
}


