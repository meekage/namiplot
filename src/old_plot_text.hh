#ifndef PLOT_TEXT_HH
#define PLOT_TEXT_HH

#include "plot_core.hh"
#include "imprint.hh"

//_________________________________________________________________________ TEXT

class text : public plot
{
private:
    coordinate_2D extents(cairo_t * cairo);
protected: /* Common virtual functions. */
    void draw(draw_context & dc) override;
    bool rebuild_caches(draw_context & dc) override;

public: /* Plot-specific constructors */
    text(text const &) = default;

    template<typename ... param_args_t>
    text(coordinate_2D xy_reference,
         std::string text_str,
         param_args_t ... param_args)
      : xy(xy_reference),
        text_string(text_str)
    {
        REGISTER_PARAMETER(align);
        REGISTER_PARAMETER(angle);
        REGISTER_PARAMETER(color);
        REGISTER_PARAMETER(size);
        plot::init(param_args...);
    }

protected: /* Plot-specific members */
    coordinate_2D xy;

    std::string text_string;

public: /* Public members */

    // The text should be printed with heading radians.
    void angle_alignment (double radians);

    float angle = 0.;
    color_t color = black;
    text_align_t align = C;
    float size = 14;
};


//______________________________________________________________ LATEX_EXCEPTION

class latex_exception : public std::runtime_error
{
public:
    const std::string latex_log;

    explicit
    latex_exception(std::string const& what_arg,
                    std::string const& latex_log_arg );
    explicit
    latex_exception(char        const* what_arg,
                    std::string const& latex_log_arg );
};


//________________________________________________________________________ LATEX

// TODO: Executors when available
class latex : public text
{
protected: /* Common virtual functions. */
    void draw(draw_context & dc) override;

public: /* Plot-specific constructors */
    latex(latex const &) = default;

    template<typename ... param_args_t>
    latex(  coordinate_2D xy_reference,
            std::string latex_math,
            param_args_t ... param_args)
      : text(xy_reference, latex_math)
    {
        REGISTER_PARAMETER(header);
        REGISTER_PARAMETER(math_style);
        plot::init(param_args...);
        texpattern = latex_to_pattern();
    }

private: /* Plot-specific members */
    PDF_pattern_t latex_to_pattern();

    PDF_pattern_t texpattern;

public: /* Public members */
    std::string header = "\\usepackage{amsmath} \n";
    math_style_t math_style = inlinemath;

};


#endif
