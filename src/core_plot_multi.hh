class plot_multi_t : public plot_base_t
{
    using vector_of_plots = std::vector< plot_ptr >;


public: /* Constructors */

    plot_multi_t() = default;

    template<typename P, typename ... Args>
        requires std::is_base_of<plot_base_t, P>::value
    plot_multi_t (P const& plot, Args ... args)
        : plot_multi_t(args...)
    {
        push_bot(plot);
    }


    template<typename ... Args>
    plot_multi_t (plot_ptr plot, Args ... args)
        : plot_multi_t(args...)
    {
        push_bot(plot);
    }


public: /* Iterators */
    // TODO const iterators, cbegin(), etc.

    class iterator : public vector_of_plots::iterator
    {
    public:
        using difference_type   = long;
        using value_type        = plot_base_t &;
        using pointer           = plot_ptr;
        using reference         = plot_base_t &;
        using iterator_category = std::random_access_iterator_tag;

        value_type operator *  ()
        { return *vector_of_plots::iterator::operator*(); }
        pointer operator -> ()
        { return *vector_of_plots::iterator::operator->(); }

        iterator(vector_of_plots::iterator it)
            : vector_of_plots::iterator(it) {}
    };

    iterator begin () { return iterator {plots.begin()};  }
    iterator end   () { return iterator {plots.end()};    }


    class reverse_iterator : public vector_of_plots::reverse_iterator
    {
    public:
        using difference_type   = long;
        using value_type        = plot_base_t &;
        using pointer           = plot_ptr;
        using reference         = plot_base_t &;
        using iterator_category = std::random_access_iterator_tag;

        value_type operator *  ()
        { return *vector_of_plots::reverse_iterator::operator*(); }
        pointer operator -> ()
        { return *vector_of_plots::reverse_iterator::operator->(); }

        reverse_iterator(vector_of_plots::reverse_iterator it)
            : vector_of_plots::reverse_iterator(it) {}
    };

    reverse_iterator rbegin () { return reverse_iterator {plots.rbegin()}; }
    reverse_iterator rend   () { return reverse_iterator {plots.rend()};   }


public:

    template <typename P> requires std::is_base_of_v<plot_base_t, P>
    void push_top (P const& plot_obj);
    void push_top (plot_ptr const& sptr);

    template <typename P> requires std::is_base_of_v<plot_base_t, P>
    void push_bot (P const& plot_obj);
    void push_bot (plot_ptr const& sptr);

    size_t size() const;

    plot_ptr const& operator[] (size_t pos) const;
    template <typename P = plot_base_t>
    std::shared_ptr<P> at(size_t pos) const;


    // ***************** Wrapping of plots.
    /* TODO
    using vector_of_plots::get_allocator;
    using vector_of_plots::at;
    using vector_of_plots::front;
    using vector_of_plots::back;
    using vector_of_plots::data;
    using vector_of_plots::cbegin;
    using vector_of_plots::cend;
    using vector_of_plots::crbegin;
    using vector_of_plots::crend;
    using vector_of_plots::empty;
    using vector_of_plots::assign;
    using vector_of_plots::pop_back;
    using vector_of_plots::clear;
    using vector_of_plots::erase;
    using vector_of_plots::resize;
    using vector_of_plots::swap;
    using vector_of_plots::insert;
    using vector_of_plots::emplace;
    using vector_of_plots::emplace_back;
    */
    // max_size
    // reserve
    // capacity
    // shrink_to_fit


private: /* Private members */

    vector_of_plots plots;


protected: /* Common virtual functions. */
    // TODO: Estas dos functiones pasan a la clase derivada 'layers'
    void draw(draw_context & cairo) const override
    {
        for (auto const& plot_p : plots)
            plot_base_t::draw(cairo, *plot_p);
    }
};


/* ********** TEMPLATE FUNCTIONS ********** */


template <typename P> 
std::shared_ptr<P>
plot_multi_t:: at(size_t pos) const
{
    for (auto const& it : plots)
    {
        std::shared_ptr<P> ptr = std::dynamic_pointer_cast<P>(it);
        if (ptr) {
            if (pos == 0)
                return ptr;
            else
                --pos;
        }
    }
    return {};
}

template <>
std::shared_ptr<plot_base_t>
plot_multi_t:: at<plot_base_t>(size_t pos) const;

template <typename P> requires std::is_base_of_v<plot_base_t, P>
void
plot_multi_t:: push_bot(P const& plot_obj)
{
    plots.push_back(std::make_shared<P>(plot_obj));
}

template <typename P> requires std::is_base_of_v<plot_base_t, P>
void
plot_multi_t:: push_top(P const& plot_obj)
{
    plots.insert(plots.begin(), std::make_shared<P>(plot_obj));
}


