template<typename T>
struct sides
{
    T lft;
    T rgt;
    T top;
    T bot;

    sides() = default;

    template <typename C>
    sides(C const& all_sides)
      : lft(all_sides), rgt(all_sides), top(all_sides), bot(all_sides)
    { }

    template <typename C>
    sides(C const& left_right, C const& top_bottom)
      : lft(left_right), rgt(left_right), top(top_bottom), bot(top_bottom)
    { }

    template <typename C>
    sides(C const& lft_arg, C const& rgt_arg,
          C const& top_arg, C const& bot_arg)
      : lft(lft_arg), rgt(rgt_arg), top(top_arg), bot(bot_arg)
    { }


    friend
    bool operator == (sides const& lhs, sides const& rhs)
    {
        return lhs.lft == rhs.lft && lhs.rgt == rhs.rgt
            && lhs.top == rhs.top && lhs.bot == rhs.bot;
    }

    friend
    bool operator != (sides const& lhs, sides const& rhs)
    { return !(lhs == rhs); }
};

