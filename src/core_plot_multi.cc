#include "namiplot"


template <>
std::shared_ptr<plot_base_t>
plot_multi_t:: at<plot_base_t>(size_t pos) const
{
    std::cout << "DEBUG: Specialization succeeded" << std::endl;
    if (pos < plots.size())
        return plots.at(pos);
    else
        return {};
}


plot_ptr const&
plot_multi_t:: operator[](size_t pos) const
{
    return plots[pos];
}

void
plot_multi_t:: push_bot(plot_ptr const& plot)
{
    plots.push_back(plot);
}

void
plot_multi_t:: push_top(plot_ptr const& plot)
{
    plots.insert(plots.begin(), plot);
}

size_t
plot_multi_t:: size() const
{
    return plots.size();
}

