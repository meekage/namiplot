// TODO Use the following to compare armadillo data types:
//if (ptr && !approx_equal(cached_value, *ptr, "reldiff", 0.0001) )

template <Namidata_armadillo DATA>
class data_t<DATA>
{
private:
    // FIXME: Use const in shared_ptr and use observer_ptr.
    //        Then get_ptr() should be casted to non-const.
    using user_ptr_t = std::variant<std::shared_ptr<DATA>, DATA const*>;
    user_ptr_t user_ptr;

    DATA const* ptr() const;

public:
    using data_type = DATA;

    data_t();
    data_t(data_t const& copy);

    data_t & operator = (data_t const& copy);


    data_t(DATA const& copy_data);
    data_t(std::shared_ptr<DATA> const& shared_data);
    data_t(DATA const* observable_data);

    data_t & operator = (DATA const& copy_data);
    data_t & operator = (std::shared_ptr<DATA> const& shared_data);
    data_t & operator = (DATA const* observable_data);

public:

    size_t size() const;
    size_t cols() const;
    size_t rows() const;
    bool   empty() const;


    std::shared_ptr<DATA> get_ptr() const;

    bool refresh() const;


    std::experimental::observer_ptr<std::mutex> mutex = nullptr;

public: /* Iterators */

    class arma_row_iterator;

    DATA::const_iterator begin() const;
    DATA::const_iterator end  () const;

    arma_row_iterator begin_row() const;
    arma_row_iterator end_row  () const;
};


/*_________________________________ ITERATORS ________________________________*/


template <Namidata_armadillo DATA>
class data_t<DATA>:: arma_row_iterator
{
    DATA const * value = nullptr;
    unsigned index_row = 0;

public:

    using difference_type   = long;
    using value_type        = arma_row_iterator;
    using pointer           = arma_row_iterator *;
    using reference         = arma_row_iterator &;
    using iterator_category = std::input_iterator_tag;


    // FIXME: why const nor value_type do not work?
    reference operator * () // const
    { return *this; }

    pointer operator -> () const
    { return this; }


    arma_row_iterator operator ++ ()
    {
        ++index_row;
        if (index_row >= value->n_rows)
            value = nullptr;
        return *this;
    }

    arma_row_iterator operator ++ (int)
    { arma_row_iterator tmp = *this; operator++(); return tmp; }


    friend bool operator == (arma_row_iterator const& lhs,
            arma_row_iterator const& rhs)
    {
        return lhs.value == rhs.value
            && (lhs.value ? lhs.index_row == rhs.index_row : true);
    }

    friend bool operator != (arma_row_iterator const& lhs,
            arma_row_iterator const& rhs)
    { return !(lhs == rhs); }


    arma_row_iterator () = default;

    arma_row_iterator (DATA const& data)
        : value(&data)
    {}

    DATA::const_row_iterator begin() const
    { return value->begin_row(index_row); }

    DATA::const_row_iterator end() const
    { return value->end_row(index_row); }
};


/*________________________________ DEFINITIONS _______________________________*/


template <Namidata_armadillo DATA>
size_t
data_t<DATA>:: cols() const
{
    return ptr()->n_cols;
}


template <Namidata_armadillo DATA>
data_t<DATA>:: data_t()
    : user_ptr(std::make_shared<DATA>( DATA{} ))
{
}


template <Namidata_armadillo DATA>
data_t<DATA>:: data_t(DATA const& copy_data)
    : user_ptr(std::make_shared<DATA>(copy_data))
{
}


template <Namidata_armadillo DATA>
data_t<DATA>:: data_t(std::shared_ptr<DATA> const& shared_data)
    : user_ptr(shared_data)
{
}


template <Namidata_armadillo DATA>
data_t<DATA>:: data_t(DATA const* observable_data)
    : user_ptr(observable_data)
{
}


template <Namidata_armadillo DATA>
data_t<DATA>:: data_t(data_t const& copy)
    : user_ptr     {copy.user_ptr},
    mutex        {copy.mutex}
{
}


template <Namidata_armadillo DATA>
bool
data_t<DATA>:: empty() const
{
    return ptr()->is_empty();
}


template <Namidata_armadillo DATA>
std::shared_ptr<DATA>
data_t<DATA>:: get_ptr() const
{
    if (user_ptr.index() == 0)
        return std::get<0>(user_ptr);
    else
        return {};
}


template <Namidata_armadillo DATA>
data_t<DATA> &
data_t<DATA>:: operator = (DATA const& copy_data)
{
    user_ptr = std::make_shared<DATA>(copy_data);
    return *this;
}


template <Namidata_armadillo DATA>
data_t<DATA> &
data_t<DATA>:: operator = (std::shared_ptr<DATA> const& shared_data)
{
    user_ptr = shared_data;
    return *this;
}


template <Namidata_armadillo DATA>
data_t<DATA> &
data_t<DATA>:: operator = (DATA const* observable_data)
{
    user_ptr = observable_data;
    return *this;
}


// FIXME Use default copy constructor in all Namidata specializations
template <Namidata_armadillo DATA>
data_t<DATA> &
data_t<DATA>:: operator = (data_t const& copy)
{
    user_ptr = copy.user_ptr;
    mutex = copy.mutex;
    return *this;
}


template <Namidata_armadillo DATA>
DATA const*
data_t<DATA>:: ptr() const
{
    if (user_ptr.index() == 0)
        return std::get<0>(user_ptr).get();
    else
        return std::get<1>(user_ptr);
}


template <Namidata_armadillo DATA>
size_t
data_t<DATA>:: rows() const
{
    return ptr()->is_empty() ? 0 : ptr()->n_rows;
}


template <Namidata_armadillo DATA>
size_t
data_t<DATA>:: size() const
{
    return ptr()->n_elem;
}


/** Iterator related **/


template <Namidata_armadillo DATA>
DATA::const_iterator
data_t<DATA>:: begin() const
{
    return ptr()->begin();
}

template <Namidata_armadillo DATA>
DATA::const_iterator
data_t<DATA>:: end() const
{
    return ptr()->end();
}


template <Namidata_armadillo DATA>
data_t<DATA>::arma_row_iterator
data_t<DATA>:: begin_row() const
{
    return arma_row_iterator(*ptr());
}

template <Namidata_armadillo DATA>
data_t<DATA>::arma_row_iterator
data_t<DATA>:: end_row() const
{
    return arma_row_iterator();
}

