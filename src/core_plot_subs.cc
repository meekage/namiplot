#include "namiplot"

#include <cmath>
#include <iostream>
#include <numeric>


// TODO: Use C++20 ranges.
std::vector<double> normalized_dims(
        std::vector<double> ratios, unsigned int const n)
{
    ratios.resize(n);
    if (n == 0)
        return ratios;

    std::for_each(ratios.begin(), ratios.end(), [](double & ratio)
    {
        if (ratio <= 0.)
        ratio = 1.;
    });

    // Clamp ratios bigger than 10x the smaller ratio.
    double max = *std::min_element(ratios.begin(), ratios.end()) * 10.;
    std::for_each(ratios.begin(), ratios.end(), [max](double & ratio)
    {
        ratio = std::min(ratio, max);
    });

    // Normalize.
    double total = std::accumulate(ratios.begin(), ratios.end(), 0.);
    double sum = 0.;
    std::for_each(ratios.begin(), ratios.end(), [total, &sum](double & ratio)
    {
        sum += ratio / total;
        ratio = sum;
    });

    ratios.insert(ratios.begin(), 0.);
    return ratios;
}

void
plot_subs_t:: draw(draw_context & cairo) const
{
    unsigned const c_rows = std::min<unsigned int>(n_rows, size());
    unsigned const c_cols = n_cols();

    std::vector ratios_h_s = normalized_dims(ratios_rows, c_rows);
    std::vector ratios_w_s = normalized_dims(ratios_cols, c_cols);

    for (unsigned j = 0; j < c_cols; j++)
	for (unsigned i = 0; i < c_rows; i++)
        {
	    unsigned sub_index = j*c_rows + i;
	    if (sub_index < size()) {
		plot_inset_t subplot{ operator[](sub_index),
                               border_width=0,
                               margins={0._pu,1._pu,0._pu,1._pu}
                               };
		subplot.margins.lft.perunit += ratios_w_s[j];
		subplot.margins.rgt.perunit -= ratios_w_s[j+1];
		subplot.margins.top.perunit += ratios_h_s[i];
		subplot.margins.bot.perunit -= ratios_h_s[i+1];
		plot_base_t::draw(cairo, subplot);
	    }
	}
}

unsigned
plot_subs_t:: n_cols() const
{
    unsigned cols = size()/n_rows;
    if (cols*n_rows != size())
	cols++;
    return cols;
}

void
plot_subs_t:: push_back(std::shared_ptr<plot_base_t> const& sptr)
{
    push_bot(sptr);
}

