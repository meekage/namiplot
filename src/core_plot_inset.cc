#include "namiplot"


void
plot_inset_t:: draw(draw_context & cairo) const
{
    double l = margins.lft.value(cairo.width);
    double r = (1._pu - margins.rgt).value(cairo.width);
    double t = margins.top.value(cairo.height);
    double b = (1._pu - margins.bot).value(cairo.height);
    double w = r - l;
    double h = b - t;

    // Plot the inset
    cairo_surface_t * draw_surface = cairo_surface_create_for_rectangle(
            cairo_get_group_target(cairo), l, t, w, h );
    draw_context inset_ctxt = { cairo_create(draw_surface), (int) w, (int) h };

    plot_base_t::draw(inset_ctxt, *inset_plot);

    cairo_destroy(inset_ctxt.cairo);
    cairo_surface_destroy(draw_surface);

    // Border
    cairo_set_source_rgba(cairo, border_color);
    cairo_set_line_width(cairo, border_width);
    cairo_rectangle(cairo, l, t, w, h);
    cairo_stroke(cairo);
}


