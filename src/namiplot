#ifndef NAMIPLOT_HH
#define NAMIPLOT_HH

#if __cplusplus <= 201709L
  #error Namiplot requires C++20
#endif


#include <atomic>
#include <cmath>
#include <filesystem>
#include <iostream>
#include <iterator>
#include <limits>
#include <memory>
#include <experimental/memory>
#include <mutex>
#include <optional>
#include <thread>
#include <type_traits>
#include <utility>
#include <variant>
#include <vector>

#include <cairo/cairo.h>
#include <cairo/cairo-xlib.h>
#include <X11/Xlib.h>

using namespace std::chrono_literals;


constexpr double pi = 3.14159265358979323846;

class plot_base_t;
using plot_ptr = std::shared_ptr<plot_base_t>;


// Order is important
#include "cairo.hh"
#include "iface_xlib.hh"
#include "iface_png.hh"
#include "utilities.hh"
#include "core_data.hh"
#include "core_data_vector.hh"
#include "core_data_vector_vector.hh"
#include "core_data_armadillo.hh"
#include "common_parameter_types.hh"
#include "parameters.hh"
#include "parameter.list"
#include "core_plot_base.hh"
#include "core_plot_multi.hh"
#include "core_plot_inset.hh"
#include "core_plot_subs.hh"
#include "frame.hh"
#include "plot_line.hh"
#include "aux_plot_ruler.hh"

#endif
