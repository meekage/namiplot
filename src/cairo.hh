
struct draw_context
{
    cairo_t * cairo;
    int width;
    int height;

    operator cairo_t * () const
    { return cairo; }
};

class cairo_substate
{
    cairo_t * cairo;
public:
    cairo_substate(cairo_substate const&) = delete;

    cairo_substate(cairo_t * cairo_context)
	: cairo(cairo_context)
    {
	cairo_save(cairo);
    }

    ~cairo_substate()
    {
	cairo_restore(cairo);
    }
};


class cairo_group
{
    cairo_t * cairo;
    cairo_pattern_t ** pattern;

public:
    cairo_group(cairo_t * cairo_context)
        : cairo(cairo_context)
        , pattern(nullptr)
    {
        if (!cairo)
            throw "namiplot:__FILE__:__LINE__:"
                  "cairo_context cannot be empty.";
	cairo_push_group(cairo);
    }

    cairo_group(cairo_t * cairo_context
               ,cairo_pattern_t * & cairo_pattern)
        : cairo(cairo_context)
        , pattern(&cairo_pattern)
    {
        if (!cairo)
            throw "namiplot:__FILE__:__LINE__:"
                  "cairo_context cannot be empty.";
        if (cairo_pattern)
            throw "namiplot:__FILE__:__LINE__:"
                  "cairo_pattern must be empty.";
	cairo_push_group(cairo);
    }

    ~cairo_group()
    {
        if (cairo)
        {
            if (pattern)
                *pattern = cairo_pop_group(cairo);
            else
            {
                cairo_pop_group_to_source(cairo);
                cairo_paint(cairo);
            }
        }
    }

    operator cairo_t * () const { return cairo; }
};

