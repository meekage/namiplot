class X11_window
{
    Display * x11_dpy;
    Atom wmDeleteMessage;
    cairo_surface_t * cairo_sfc;

    plot_base_t & pl;
    std::chrono::microseconds update_period;
    std::atomic<bool> active;

    void thread_main();

public:
    X11_window() = delete;

    X11_window(plot_base_t & p,
               int win_width,
               int win_height);

    X11_window(plot_base_t & p,
               std::chrono::microseconds period,
               int win_width,
               int win_height);

    X11_window(plot_base_t & p,
               float frequency,
               int win_width,
               int win_height);

    ~X11_window();

    inline bool is_active() { return active; }
};
