#include <armadillo> // Matrix library
#include <namiplot>

int
main()
{
    nami::quick_scatter(arma::random(5,1), symbol=star);

    using namespace nami;
    using namespace arma;

    mat D = random(5);

    // Data tracked by namiplot when passed by reference.
    line trajectory { &D, width=2., color=black};
    multiplot graph = { trajectory, scatter { &D } };

    // Adjust some plot options after construction.
    graph[1].symbol  = star;
    trajectory.color = red;

    window window {640,480};
    //window.render(graph); // Just plot once.
    window.freq_rate(graph, 60); // Plot with a frequency of 60Hz.

    #include "model_syntax.cc.nami"

    // Plot window automatically updates... with its own thread! :)
    while (1)
	D += random(D.size());
}

void plot_setup()
{
    using namespace nami;

    line trajectory {D, width=2};

    quick_plot(trajectory, line{&D2, scale=autox, color=red});
    

    // Propiedades de los properties:

    line l {data, width=1.}
    line.width = 1.;

    rs = render_screen(line);
    rs.update();
    line.width = 2.;
    rs.update();


}

concept parameter_T;

template<typename parameter_T T>
class standard_parameter
{
public:
    mutable T cache_value;

    bool update() const
    {
        // Width tiene una cache privada:
        if (this->width != this->width_cache)
        {
            this->width_cache = this->width;
            return true;
        }

        // A veces el parametro debe ser calculado, pero solo si no esta
        // fijado por el usuario. En este caso, el valor de retorno depende
        // de otras variables.
    }

private:
    T const* user_value;
}

render_screen_t::update()
{
    template foreach parameter
        parameter.update();

}
